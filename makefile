VERSION := 1.0

# project

tidy:
	go mod tidy
	go mod vendor

test:
	go test ./... -count=1
	staticcheck -checks=all ./...


# admin

genKey:
	go run app/tooling/admin/main.go genKey

# kubernetes

getAllPods:
	kubectl get pods --all-namespaces   

# kubectl exec --stdin --tty <podID>   -- /bin/sh

# docker

docker-build:
	docker build \
			-f conf/docker/dockerfile.family-budget \
			-t family-budget-amd64:$(VERSION) \
			--build-arg BUILD_REF=$(VERSION) \
			--build-arg BUILD_DATE=`date -u +"%Y-%m-%dT%H:%M:%SZ"` \
			.

# kind

KIND_CLUSTER := fb-cluster
FB_NAMESPACE := fb-system

kind-up:
	kind create cluster \
		--image kindest/node:v1.25.2@sha256:9be91e9e9cdf116809841fc77ebdb8845443c4c72fe5218f3ae9eb57fdb4bace \
		--name $(KIND_CLUSTER) \
		--config conf/k8s/kind/kind-config.yaml
	kubectl config set-context --current --namespace=${FB_NAMESPACE}

kind-down:
	kind delete cluster --name $(KIND_CLUSTER)

kind-load:
	cd conf/k8s/kind/fb-pod; kustomize edit set image fb-api-image=family-budget-amd64:$(VERSION) 
	kind load docker-image family-budget-amd64:$(VERSION) --name $(KIND_CLUSTER)

kind-apply:
	kustomize build conf/k8s/kind/database-pod | kubectl apply -f -
	kubectl wait --namespace=database-system --timeout=120s --for=condition=Available deployment/database-pod
	kustomize build conf/k8s/kind/fb-pod | kubectl apply -f -

kind-status:
	kubectl get nodes -o wide
	kubectl get svc -o wide 
	kubectl get pods -o wide --watch --all-namespaces

# default namespace in kind-up
kind-status-fb:
	kubectl get pods -o wide --watch 

kind-status-db:
	kubectl get pods -o wide --watch --namespace=database-system

kind-logs:
	kubectl logs -l app=fb --all-containers=true -f --tail=100 | go run app/tooling/logfmt/main.go 

kind-restart:
	kubectl rollout restart deployment fb-pod 

kind-update: docker-build kind-load kind-restart

kind-update-apply: docker-build kind-load kind-apply


# Frontend

front-run:
	npm start --prefix web/angular-app

front-install:
	npm i --prefix web/angular-app

