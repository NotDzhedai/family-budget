# Family-budget

## TODO:

- ✅ FRONTEND add error handling to overview and new pages
- ⬜️ FRONTEND add total stats by period of time
- ✅ FRONTEND update addresses
- ⬜️ FRONTEND update payments
- ⬜️ BACKEND complete tests for user controllers

## Requirements:

- KIND [install](https://kind.sigs.k8s.io/docs/user/quick-start/)
- kubectl [install](https://kubernetes.io/docs/tasks/tools/)
- Kustomize [install](https://kubectl.docs.kubernetes.io/installation/kustomize/)
- Golang [install](https://go.dev/doc/install)

## Run project:

### Run backend with kind

```
make kind-up
make kind-update-apply
```

- api-port: 3000
- debug-port: 4000

### Stop backend

```
make kind-down
```

### Run frontend (angular)

```
make front-install
make front-run
```

- front-port: 5000
