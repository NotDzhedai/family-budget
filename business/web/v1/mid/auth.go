package mid

import (
	"context"
	"errors"
	"net/http"
	"strings"

	"gitlab.com/NotDzhedai/family-budget/business/sys/auth"
	v1web "gitlab.com/NotDzhedai/family-budget/business/web/v1"
	"gitlab.com/NotDzhedai/family-budget/foundation/web"
)

// Authenticate validates a JWT from the `Authorization` header.
func Authenticate(a *auth.Auth) web.Middleware {
	// This is the actual middleware function to be executed.
	m := func(handler web.Handler) web.Handler {
		// Create the handler that will be attached in the middleware chain.
		h := func(ctx context.Context, w http.ResponseWriter, r *http.Request) error {
			// Expecting: bearer <token>
			authStr := r.Header.Get("authorization")
			// Parse the authorization header.
			parts := strings.Split(authStr, " ")
			if len(parts) != 2 || strings.ToLower(parts[0]) != "bearer" {
				err := errors.New("expected authorization header formar: bearer <token>")
				return v1web.NewRequestError(err, http.StatusUnauthorized)
			}
			// Validate the token is signed by us.
			claims, err := a.ValidateToken(parts[1])
			if err != nil {
				return v1web.NewRequestError(err, http.StatusUnauthorized)
			}
			// Add claims to the context so they can be retrieved later.
			ctx = auth.SetClaims(ctx, claims)
			// Call the next handler.
			return handler(ctx, w, r)
		}
		return h
	}
	return m
}
