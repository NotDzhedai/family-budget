// Package payment provides a core business API for payments.
package payment

import (
	"context"
	"errors"
	"fmt"
	"math"
	"time"

	"github.com/jmoiron/sqlx"
	"gitlab.com/NotDzhedai/family-budget/business/core/payment/db"
	"gitlab.com/NotDzhedai/family-budget/business/sys/database"
	"gitlab.com/NotDzhedai/family-budget/business/sys/validate"
	"go.uber.org/zap"
)

var (
	ErrNotFound              = errors.New("not found")
	ErrInvalidID             = errors.New("ID is not in its proper form")
	ErrAuthenticationFailure = errors.New("authentication failed")
	ErrDublicateKey          = errors.New("already exists")
	ErrNotFoundForeignKey    = errors.New("not found address with such ID")
)

type Core struct {
	store db.Store
}

func NewCore(log *zap.SugaredLogger, sqlxDB *sqlx.DB) Core {
	return Core{
		store: db.NewStore(log, sqlxDB),
	}
}

func (c Core) Create(ctx context.Context, np NewPayment, userID string, now time.Time) (Payment, error) {
	if err := validate.Check(np); err != nil {
		return Payment{}, fmt.Errorf("validating data: %w", err)
	}

	dbPayment := db.Payment{
		ID:                     validate.GenerateID(),
		UserID:                 userID,
		AddressID:              np.AddressID,
		DateCreated:            now,
		Gas:                    np.Gas,
		GasTransit:             np.GasTransit,
		Light:                  np.Light,
		Water:                  np.Water,
		Waste:                  np.Waste,
		ApartmentFee:           np.ApartmentFee,
		Elevator:               np.Elevator,
		Intercom:               np.Intercom,
		HeatingFee:             np.HeatingFee,
		HeatingSubscriptionFee: np.HeatingSubscriptionFee,
	}

	if err := c.store.Create(ctx, dbPayment); err != nil {
		switch {
		case errors.Is(err, database.ErrDublicateKey):
			return Payment{}, ErrDublicateKey
		case errors.Is(err, database.ErrNotFoundForeignKey):
			return Payment{}, ErrNotFoundForeignKey
		default:
			return Payment{}, fmt.Errorf("create: %w", err)
		}
	}

	return Payment(dbPayment), nil
}

func (c Core) QueryByID(ctx context.Context, pmtID, userID string) (Payment, error) {
	if err := validate.CheckID(pmtID); err != nil {
		return Payment{}, ErrInvalidID
	}

	dbPmt, err := c.store.QueryByID(ctx, pmtID, userID)
	if err != nil {
		if errors.Is(err, database.ErrDBNotFound) {
			return Payment{}, ErrNotFound
		}
		return Payment{}, fmt.Errorf("query by id: %w", err)
	}

	return Payment(dbPmt), nil
}

func (c Core) Delete(ctx context.Context, pmtID, userID string) error {
	if err := validate.CheckID(pmtID); err != nil {
		return ErrInvalidID
	}

	if err := c.store.Delete(ctx, pmtID, userID); err != nil {
		if errors.Is(err, database.ErrDBNotFound) {
			return ErrNotFound
		}
		return fmt.Errorf("delete: %w", err)
	}

	return nil
}

func (c Core) Query(ctx context.Context, userID, addrID string, pageNumber int, rowsPerPage int) ([]Payment, error) {
	if err := validate.CheckID(addrID); err != nil {
		return nil, ErrInvalidID
	}

	dbPmts, err := c.store.QueryByAddress(ctx, userID, addrID, pageNumber, rowsPerPage)
	if err != nil {
		return nil, fmt.Errorf("query: %w", err)
	}

	return toPaymentSlice(dbPmts), nil
}

// GetPaymentsPagesLen get pages count for payments for pagination
func (c Core) GetPaymentsPagesLen(ctx context.Context, userID, addrID string, rowsPerPage int) (int, error) {
	paymentsCount, err := c.store.GetPaymentsPagesLen(ctx, userID, addrID)
	if err != nil {
		return 0, fmt.Errorf("query payments len: %w", err)
	}

	pages := math.Ceil(float64(paymentsCount) / float64(rowsPerPage))
	return int(pages), nil
}

func (c Core) Update(ctx context.Context, pmtID, userID string, up UpdatePayment, now time.Time) error {
	if err := validate.CheckID(pmtID); err != nil {
		return ErrInvalidID
	}
	if err := validate.Check(up); err != nil {
		return fmt.Errorf("validating data: %w", err)
	}

	dbPmt, err := c.store.QueryByID(ctx, pmtID, userID)
	if err != nil {
		if errors.Is(err, database.ErrDBNotFound) {
			return ErrNotFound
		}
		return fmt.Errorf("updating user userID[%s]: %w", userID, err)
	}

	if up.AddressID != nil {
		dbPmt.AddressID = *up.AddressID
	}
	if up.Gas != nil {
		dbPmt.Gas = *up.Gas
	}
	if up.GasTransit != nil {
		dbPmt.GasTransit = *up.GasTransit
	}
	if up.Light != nil {
		dbPmt.Light = *up.Light
	}
	if up.Water != nil {
		dbPmt.Water = *up.Water
	}
	if up.Waste != nil {
		dbPmt.Waste = *up.Waste
	}
	if up.ApartmentFee != nil {
		dbPmt.ApartmentFee = *up.ApartmentFee
	}
	if up.Elevator != nil {
		dbPmt.Elevator = *up.Elevator
	}
	if up.Intercom != nil {
		dbPmt.Intercom = *up.Intercom
	}
	if up.HeatingFee != nil {
		dbPmt.HeatingFee = *up.HeatingFee
	}
	if up.HeatingSubscriptionFee != nil {
		dbPmt.HeatingSubscriptionFee = *up.HeatingSubscriptionFee
	}

	// dbPmt.DateUpdated = now

	if err := c.store.Update(ctx, dbPmt); err != nil {
		if errors.Is(err, database.ErrDBNotFound) {
			return ErrNotFound
		}
		return fmt.Errorf("update: %w", err)
	}

	return nil
}
