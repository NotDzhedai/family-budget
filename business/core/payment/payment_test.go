package payment_test

import (
	"context"
	"errors"
	"fmt"
	"testing"
	"time"

	"github.com/google/go-cmp/cmp"
	"gitlab.com/NotDzhedai/family-budget/business/core/address"
	"gitlab.com/NotDzhedai/family-budget/business/core/payment"
	"gitlab.com/NotDzhedai/family-budget/business/core/user"
	"gitlab.com/NotDzhedai/family-budget/business/data/dbschema"
	"gitlab.com/NotDzhedai/family-budget/business/data/dbtests"
	"gitlab.com/NotDzhedai/family-budget/foundation/docker"
)

var c *docker.Container

func TestMain(m *testing.M) {
	var err error
	c, err = dbtests.StartDB()
	if err != nil {
		fmt.Println(err)
		return
	}
	defer dbtests.StopDB(c)

	m.Run()
}

const migrationPath = "file://../../data/dbschema/sql/migrations"

func TestPayment(t *testing.T) {
	log, db, teardown := dbtests.NewUnit(t, c, "testpayment", migrationPath)
	t.Cleanup(teardown)

	userCore := user.NewCore(log, db)
	addressCore := address.NewCore(log, db)
	paymentCore := payment.NewCore(log, db)

	t.Log("Given the need to work with payment records")
	{
		// prepare for test addresses
		t.Log("Create user")
		ctx := context.Background()
		now := time.Date(2018, time.October, 1, 0, 0, 0, 0, time.UTC)

		nu := user.NewUser{
			Name:            "Test User",
			Email:           "test@gmail.com",
			Password:        "qwerty",
			PasswordConfirm: "qwerty",
		}

		// already tested in users.
		// need for creating address and payments
		usr, _ := userCore.Create(ctx, nu, now)
		// already tested in addresses
		// create address
		const AddressName = "Test Address"
		na := address.NewAddress{
			Address: AddressName,
		}

		addr, _ := addressCore.Create(ctx, usr.ID, na, now)

		// test create payment
		np := payment.NewPayment{
			AddressID:              addr.ID,
			Gas:                    10,
			GasTransit:             11.1,
			Light:                  12.2,
			Water:                  13.3,
			Waste:                  14.4,
			ApartmentFee:           100.10,
			Elevator:               110.11,
			Intercom:               200.2,
			HeatingFee:             122.43,
			HeatingSubscriptionFee: 1232.234,
		}
		pmt, err := paymentCore.Create(ctx, np, usr.ID, now)
		if err != nil {
			t.Fatalf("\t%s\tTest:\tShould be able to create payment : %s.", dbtests.Failed, err)
		}
		t.Logf("\t%s\tTest:\tShould be able to create payment.", dbtests.Success)

		// test get payment by id
		pmtByID, err := paymentCore.QueryByID(ctx, pmt.ID, pmt.UserID)
		if err != nil {
			t.Fatalf("\t%s\tTest:\tShould be able to get payment by id: %s.", dbtests.Failed, err)
		}
		t.Logf("\t%s\tTest:\tShould be able to get payment by id", dbtests.Success)

		if diff := cmp.Diff(pmt, pmtByID); diff != "" {
			t.Fatalf("\t%s\tTest:\tShould get back the same payment: %s.", dbtests.Failed, err)
		}
		t.Logf("\t%s\tTest:\tShould get back the same payment", dbtests.Success)

		// test update payment
		up := payment.UpdatePayment{
			Gas:   dbtests.FloatPointer(100),
			Light: dbtests.FloatPointer(200),
		}

		if err := paymentCore.Update(ctx, pmt.ID, pmt.UserID, up, now); err != nil {
			t.Fatalf("\t%s\tTest %d:\tShould be able to update payment", dbtests.Failed, err)
		}

		saved, err := paymentCore.QueryByID(ctx, pmt.ID, pmt.UserID)
		if err != nil {
			t.Fatalf("\t%s\tTest:\tShould be able to get updated payment by id: %s.", dbtests.Failed, err)
		}
		t.Logf("\t%s\tTest:\tShould be able to get updated payment by id", dbtests.Success)

		if saved.Gas != *up.Gas {
			t.Errorf("\t%s\tTest:\tShould be able to see updates to Gas.", dbtests.Failed)
			t.Logf("\t\tTest:\tGot: %v", saved.Gas)
			t.Logf("\t\tTest:\tExp: %v", *up.Gas)
		} else {
			t.Logf("\t%s\tTest:\tShould be able to see updates to Name.", dbtests.Success)
		}

		if saved.Light != *up.Light {
			t.Errorf("\t%s\tTest:\tShould be able to see updates to Gas.", dbtests.Failed)
			t.Logf("\t\tTest:\tGot: %v", saved.Light)
			t.Logf("\t\tTest:\tExp: %v", *up.Light)
		} else {
			t.Logf("\t%s\tTest:\tShould be able to see updates to Name.", dbtests.Success)
		}

		if err := paymentCore.Delete(ctx, saved.ID, usr.ID); err != nil {
			t.Fatalf("\t%s\tTest:\tShould be able to delete payment : %s.", dbtests.Failed, err)
		}
		t.Logf("\t%s\tTest:\tShould be able to delete payment", dbtests.Success)

		_, err = paymentCore.QueryByID(ctx, saved.ID, usr.ID)
		if !errors.Is(err, payment.ErrNotFound) {
			t.Fatalf("\t%s\tTest:\tShould NOT be able to retrieve payment : %s.", dbtests.Failed, err)
		}
		t.Logf("\t%s\tTest:\tShould NOT be able to retrieve user.", dbtests.Success)

		// test payment with other user
		nu2 := user.NewUser{
			Name:            "Test User2",
			Email:           "test2@gmail.com",
			Password:        "qwerty2",
			PasswordConfirm: "qwerty2",
		}
		usr2, _ := userCore.Create(ctx, nu2, now)

		_, err = paymentCore.QueryByID(ctx, saved.ID, usr2.ID)
		if !errors.Is(err, payment.ErrNotFound) {
			t.Fatalf("\t%s\tTest:\tShould NOT be able to retrieve payment with user 2 : %s.", dbtests.Failed, err)
		}
		t.Logf("\t%s\tTest:\tShould NOT be able to retrieve payment with user 2 ", dbtests.Success)
	}
}

func TestPagingPayment(t *testing.T) {
	log, db, teardown := dbtests.NewUnit(t, c, "testpaging_payment", migrationPath)
	t.Cleanup(teardown)

	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	dbschema.Seed(ctx, db)

	// from db seed
	const UserID = "5cf37266-3473-4006-984f-9325122678b7"
	const AddressID = "76b983d2-8c49-469c-864d-2c950d33d2d2"

	paymentCore := payment.NewCore(log, db)

	t.Log("Given the need to page through payment records.")
	{
		payment1, err := paymentCore.Query(ctx, UserID, AddressID, 1, 1)
		if err != nil {
			t.Fatalf("\t%s\tTest:\tShould be able to retrieve payments for page 1 : %s.", dbtests.Failed, err)
		}
		t.Logf("\t%s\tTest:\tShould be able to retrieve payments for page 1.", dbtests.Success)

		if len(payment1) != 1 {
			t.Fatalf("\t%s\tTest:\tShould have a single user : %s.", dbtests.Failed, err)
		}
		t.Logf("\t%s\tTest:\tShould have a single user.", dbtests.Success)

		payment2, err := paymentCore.Query(ctx, UserID, AddressID, 2, 1)
		if err != nil {
			t.Fatalf("\t%s\tTest:\tShould be able to retrieve payments for page 2 : %s.", dbtests.Failed, err)
		}
		t.Logf("\t%s\tTest:\tShould be able to retrieve payments for page 2.", dbtests.Success)

		if len(payment2) != 1 {
			t.Fatalf("\t%s\tTest:\tShould have a single user : %s.", dbtests.Failed, err)
		}
		t.Logf("\t%s\tTest:\tShould have a single user.", dbtests.Success)

		if payment1[0].ID == payment2[0].ID {
			t.Logf("\t\tTest:\tPayment1: %v", payment1[0].ID)
			t.Logf("\t\tTest:\tPayment2: %v", payment2[0].ID)
			t.Fatalf("\t%s\tTest:\tShould have different payments : %s.", dbtests.Failed, err)
		}
		t.Logf("\t%s\tTest:\tShould have different payments.", dbtests.Success)
	}
}
