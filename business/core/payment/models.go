package payment

import (
	"time"

	"gitlab.com/NotDzhedai/family-budget/business/core/payment/db"
)

type Payment struct {
	ID                     string    `json:"payment_id"`
	UserID                 string    `json:"user_id"`
	AddressID              string    `json:"address_id"`
	DateCreated            time.Time `json:"date_created"`
	Gas                    float64   `json:"gas"`
	GasTransit             float64   `json:"gas_transit"`
	Light                  float64   `json:"light"`
	Water                  float64   `json:"water"`
	Waste                  float64   `json:"waste"`
	ApartmentFee           float64   `json:"apartment_fee"`
	Elevator               float64   `json:"elevator"`
	Intercom               float64   `json:"intercom"`
	HeatingFee             float64   `json:"heating_fee"`
	HeatingSubscriptionFee float64   `json:"heating_subscription_fee"`
}

type NewPayment struct {
	AddressID              string  `json:"address_id" validate:"required"`
	Gas                    float64 `json:"gas" validate:"required,gte=0"`
	GasTransit             float64 `json:"gas_transit" validate:"required,gte=0"`
	Light                  float64 `json:"light" validate:"required,gte=0"`
	Water                  float64 `json:"water" validate:"required,gte=0"`
	Waste                  float64 `json:"waste" validate:"required,gte=0"`
	ApartmentFee           float64 `json:"apartment_fee" validate:"required,gte=0"`
	Elevator               float64 `json:"elevator" validate:"required,gte=0"`
	Intercom               float64 `json:"intercom" validate:"required,gte=0"`
	HeatingFee             float64 `json:"heating_fee" validate:"required,gte=0"`
	HeatingSubscriptionFee float64 `json:"heating_subscription_fee" validate:"required,gte=0"`
}

type UpdatePayment struct {
	AddressID              *string  `json:"address_id"`
	Gas                    *float64 `json:"gas" validate:"omitempty,gte=0"`
	GasTransit             *float64 `json:"gas_transit" validate:"omitempty,gte=0"`
	Light                  *float64 `json:"light" validate:"omitempty,gte=0"`
	Water                  *float64 `json:"water" validate:"omitempty,gte=0"`
	Waste                  *float64 `json:"waste" validate:"omitempty,gte=0"`
	ApartmentFee           *float64 `json:"apartment_fee" validate:"omitempty,gte=0"`
	Elevator               *float64 `json:"elevator" validate:"omitempty,gte=0"`
	Intercom               *float64 `json:"intercom" validate:"omitempty,gte=0"`
	HeatingFee             *float64 `json:"heating_fee" validate:"omitempty,gte=0"`
	HeatingSubscriptionFee *float64 `json:"heating_subscription_fee" validate:"omitempty,gte=0"`
}

func toPaymentSlice(dbPmts []db.Payment) []Payment {
	pmts := make([]Payment, len(dbPmts))
	for i, dbUsr := range dbPmts {
		pmts[i] = Payment(dbUsr)
	}
	return pmts
}
