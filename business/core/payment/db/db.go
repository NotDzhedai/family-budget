// Package db contains payment related CRUD functionality.
package db

import (
	"context"
	"fmt"

	"github.com/jmoiron/sqlx"
	"gitlab.com/NotDzhedai/family-budget/business/sys/database"
	"go.uber.org/zap"
)

// Store manages the set of API's for user access.
type Store struct {
	log          *zap.SugaredLogger
	tr           database.Transactor
	dbctx        sqlx.ExtContext
	isWithinTran bool
}

// NewStore constructs a data for api access.
func NewStore(log *zap.SugaredLogger, db *sqlx.DB) Store {
	return Store{
		log:   log,
		dbctx: db,
		tr:    db,
	}
}

// WithinTran runs passed function and do commit/rollback at the end.
func (s Store) WithinTran(ctx context.Context, fn func(sqlx.ExtContext) error) error {
	if s.isWithinTran {
		return fn(s.dbctx)
	}
	return database.WithinTran(ctx, s.log, s.tr, fn)
}

// Tran return new Store with transaction in it.
func (s Store) Tran(tx sqlx.ExtContext) Store {
	return Store{
		log:          s.log,
		tr:           s.tr,
		dbctx:        tx,
		isWithinTran: true,
	}
}

// Create inserts a new payment into the database.
func (s Store) Create(ctx context.Context, payment Payment) error {
	const q = `
	INSERT INTO utility_payments
		(payment_id, user_id, address_id, 
		 date_created, gas, gas_transit, 
		 light, water, waste, 
		 apartment_fee, elevator, intercom, 
		 heating_fee, heating_subscription_fee)
	VALUES
		(:payment_id, :user_id, :address_id, 
		 :date_created, :gas, :gas_transit, 
		 :light, :water, :waste, 
		 :apartment_fee, :elevator, :intercom, 
		 :heating_fee, :heating_subscription_fee)
	`

	if err := database.NamedExecContext(ctx, s.log, s.dbctx, q, payment); err != nil {
		return fmt.Errorf("inserting payment: %w", err)
	}

	return nil
}

// QueryByID gets the specified payment from the database.
func (s Store) QueryByID(ctx context.Context, pmtID, userID string) (Payment, error) {
	data := struct {
		PaymentID string `db:"payment_id"`
		UserID    string `db:"user_id"`
	}{
		PaymentID: pmtID,
		UserID:    userID,
	}

	const q = `
	SELECT
		*
	FROM
		utility_payments
	WHERE 
		payment_id = :payment_id AND user_id = :user_id
	`

	var pmt Payment
	if err := database.NamedQueryStruct(ctx, s.log, s.dbctx, q, data, &pmt); err != nil {
		return Payment{}, fmt.Errorf("selecting paymentID[%q]: %w", pmtID, err)
	}

	return pmt, nil
}

// Delete removes a payment from the database.
func (s Store) Delete(ctx context.Context, pmtID, userID string) error {
	data := struct {
		PaymentID string `db:"payment_id"`
		UserID    string `db:"user_id"`
	}{
		PaymentID: pmtID,
		UserID:    userID,
	}

	const q = `
	DELETE FROM 
		utility_payments
	WHERE
		payment_id = :payment_id AND user_id = :user_id
	`
	if err := database.NamedExecContext(ctx, s.log, s.dbctx, q, data); err != nil {
		return fmt.Errorf("deleting paymentID[%s]: %w", pmtID, err)
	}

	return nil
}

// QueryByAddress retrieves a list of existing payments by address from the database.
func (s Store) QueryByAddress(ctx context.Context, userID, addrID string, pageNumber int, rowsPerPage int) ([]Payment, error) {
	data := struct {
		Offset      int    `db:"offset"`
		RowsPerPage int    `db:"rows_per_page"`
		AddressID   string `db:"address_id"`
		UserID      string `db:"user_id"`
	}{
		Offset:      (pageNumber - 1) * rowsPerPage,
		RowsPerPage: rowsPerPage,
		AddressID:   addrID,
		UserID:      userID,
	}

	const q = `
	SELECT 
		*
	FROM
		utility_payments
	WHERE 
		address_id = :address_id AND user_id = :user_id
	ORDER BY 
		date_created
	OFFSET :offset ROWS FETCH NEXT :rows_per_page ROWS ONLY
	`

	var pmts []Payment
	if err := database.NamedQuerySlice(ctx, s.log, s.dbctx, q, data, &pmts); err != nil {
		return nil, fmt.Errorf("selecting payments: %w", err)
	}

	return pmts, nil
}

func (s Store) GetPaymentsPagesLen(ctx context.Context, userID, addrID string) (int, error) {
	data := struct {
		AddressID string `db:"address_id"`
		UserID    string `db:"user_id"`
	}{
		AddressID: addrID,
		UserID:    userID,
	}

	const q = `
	SELECT 
		count(*)
	FROM
		utility_payments
	WHERE 
		address_id = :address_id AND user_id = :user_id
	`

	var pmtsLen int
	if err := database.NamedQuery(ctx, s.log, s.dbctx, q, data, &pmtsLen); err != nil {
		return 0, fmt.Errorf("selecting payments len: %w", err)
	}

	return pmtsLen, nil
}

// Update replaces a payment document in the database.
func (s Store) Update(ctx context.Context, pmt Payment) error {
	const q = `
	UPDATE
		utility_payments
	SET
		address_id = :address_id,
		date_created = :date_created,
		gas = :gas,
		gas_transit = :gas_transit, 
		light = :light, 
		water = :water, 
		waste = :waste, 
		apartment_fee = :apartment_fee, 
		elevator = :elevator, 
		intercom = :intercom, 
		heating_fee = :heating_fee, 
		heating_subscription_fee = :heating_subscription_fee
	WHERE
		payment_id = :payment_id
	`

	if err := database.NamedExecContext(ctx, s.log, s.dbctx, q, pmt); err != nil {
		return fmt.Errorf("updating paymentID[%s]: %w", pmt.ID, err)
	}

	return nil
}
