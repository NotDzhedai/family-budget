package db

import (
	"time"
)

type Payment struct {
	ID                     string    `db:"payment_id"`
	UserID                 string    `db:"user_id"`
	AddressID              string    `db:"address_id"`
	DateCreated            time.Time `db:"date_created"`
	Gas                    float64   `db:"gas"`
	GasTransit             float64   `db:"gas_transit"`
	Light                  float64   `db:"light"`
	Water                  float64   `db:"water"`
	Waste                  float64   `db:"waste"`
	ApartmentFee           float64   `db:"apartment_fee"`
	Elevator               float64   `db:"elevator"`
	Intercom               float64   `db:"intercom"`
	HeatingFee             float64   `db:"heating_fee"`
	HeatingSubscriptionFee float64   `db:"heating_subscription_fee"`
}
