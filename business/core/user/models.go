package user

import (
	"time"

	"gitlab.com/NotDzhedai/family-budget/business/core/user/db"
)

// User represents an individual user.
type User struct {
	ID           string    `json:"id"`
	Name         string    `json:"name"`
	Email        string    `json:"email"`
	PasswordHash []byte    `json:"-"`
	DateCreated  time.Time `json:"date_created"`
	DateUpdated  time.Time `json:"date_updated"`
}

// NewUser contains information needed to create a new User.
type NewUser struct {
	Name            string `json:"name" validate:"required,min=4"`
	Email           string `json:"email" validate:"required,email"`
	Password        string `json:"password" validate:"required,min=5"`
	PasswordConfirm string `json:"password_validation" validate:"eqfield=Password"`
}

// UpdateUser defines what information may be provided to modify an existing
// User. All fields are optional so clients can send just the fields they want
// changed. It uses pointer fields so we can differentiate between a field that
// was not provided and a field that was provided as explicitly blank. Normally
// we do not want to use pointers to basic types but we make exceptions around
// marshalling/unmarshalling.
type UpdateUser struct {
	Name            *string `json:"name" validate:"omitempty,min=4"`
	Email           *string `json:"email" validate:"omitempty,email"`
	Password        *string `json:"password" validate:"omitempty,min=5"`
	PasswordConfirm *string `json:"password_validation" validate:"omitempty,eqfield=Password"`
}

// ==========

func toUserSlice(db []db.User) []User {
	res := make([]User, len(db))
	for i, dbItem := range db {
		res[i] = User(dbItem)
	}
	return res
}
