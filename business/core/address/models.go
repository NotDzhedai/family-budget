package address

import (
	"time"

	"gitlab.com/NotDzhedai/family-budget/business/core/address/db"
)

// Address represents an individual user.
type Address struct {
	ID          string    `json:"address_id"`
	UserID      string    `json:"user_id"`
	Address     string    `json:"address"`
	DateCreated time.Time `json:"date_created"`
	DateUpdated time.Time `json:"date_updated"`
}

// NewAddress contains information needed to create a new User.
type NewAddress struct {
	Address string `json:"address" validate:"required,min=4"`
}

type UpdateAddress struct {
	Address string `json:"address" validate:"required,min=4"`
}

// ==========

func toAddressSlice(dbAddrs []db.Address) []Address {
	addrs := make([]Address, len(dbAddrs))
	for i, dbAddr := range dbAddrs {
		addrs[i] = Address(dbAddr)
	}
	return addrs
}
