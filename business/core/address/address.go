// Package address provides a core business API for addresses.
package address

import (
	"context"
	"errors"
	"fmt"
	"time"

	"github.com/jmoiron/sqlx"
	"gitlab.com/NotDzhedai/family-budget/business/core/address/db"
	"gitlab.com/NotDzhedai/family-budget/business/sys/database"
	"gitlab.com/NotDzhedai/family-budget/business/sys/validate"
	"go.uber.org/zap"
)

var (
	ErrNotFound              = errors.New("not found")
	ErrInvalidID             = errors.New("ID is not in its proper form")
	ErrAuthenticationFailure = errors.New("authentication failed")
	ErrDublicateKey          = errors.New("already exists")
)

type Core struct {
	store db.Store
}

func NewCore(log *zap.SugaredLogger, sqlxDB *sqlx.DB) Core {
	return Core{
		store: db.NewStore(log, sqlxDB),
	}
}

func (c Core) Create(ctx context.Context, userID string, na NewAddress, now time.Time) (Address, error) {
	if err := validate.Check(na); err != nil {
		return Address{}, fmt.Errorf("validating data: %w", err)
	}

	dbAddr := db.Address{
		ID:          validate.GenerateID(),
		UserID:      userID,
		Address:     na.Address,
		DateCreated: now,
		DateUpdated: now,
	}

	if err := c.store.Create(ctx, dbAddr); err != nil {
		if errors.Is(err, database.ErrDublicateKey) {
			return Address{}, ErrDublicateKey
		}
		return Address{}, fmt.Errorf("create: %w", err)
	}

	return Address(dbAddr), nil
}

func (c Core) QueryByID(ctx context.Context, addrID, userID string) (Address, error) {
	if err := validate.CheckID(addrID); err != nil {
		return Address{}, ErrInvalidID
	}

	dbAddr, err := c.store.QueryByID(ctx, addrID, userID)
	if err != nil {
		if errors.Is(err, database.ErrDBNotFound) {
			return Address{}, ErrNotFound
		}
		return Address{}, fmt.Errorf("query by id: %w", err)
	}

	return Address(dbAddr), nil
}

func (c Core) QueryByAddress(ctx context.Context, addr, userID string) (Address, error) {
	dbAddr, err := c.store.QueryByAddress(ctx, addr, userID)
	if err != nil {
		if errors.Is(err, database.ErrDBNotFound) {
			return Address{}, ErrNotFound
		}
		return Address{}, fmt.Errorf("query by address: %w", err)
	}

	return Address(dbAddr), nil
}

func (c Core) Delete(ctx context.Context, addrID, userID string) error {
	if err := validate.CheckID(addrID); err != nil {
		return ErrInvalidID
	}

	if err := c.store.Delete(ctx, addrID, userID); err != nil {
		if errors.Is(err, database.ErrDBNotFound) {
			return ErrNotFound
		}
		return fmt.Errorf("delete: %w", err)
	}

	return nil
}

func (c Core) Update(ctx context.Context, addrID, userID string, ua UpdateAddress, now time.Time) error {
	if err := validate.Check(ua); err != nil {
		return fmt.Errorf("validating data: %w", err)
	}

	if err := validate.CheckID(addrID); err != nil {
		return ErrInvalidID
	}

	dbAddr, err := c.store.QueryByID(ctx, addrID, userID)
	if err != nil {
		if errors.Is(err, database.ErrDBNotFound) {
			return ErrNotFound
		}
		return fmt.Errorf("updating user userID[%s]: %w", userID, err)
	}

	dbAddr.Address = ua.Address
	dbAddr.DateUpdated = now

	if err := c.store.Update(ctx, dbAddr); err != nil {
		if errors.Is(err, database.ErrDBNotFound) {
			return ErrNotFound
		}
		return fmt.Errorf("update: %w", err)
	}

	return nil
}

func (c Core) Query(ctx context.Context, userID string, pageNumber int, rowsPerPage int) ([]Address, error) {
	dbAddrs, err := c.store.Query(ctx, userID, pageNumber, rowsPerPage)
	if err != nil {
		return nil, fmt.Errorf("query: %w", err)
	}

	return toAddressSlice(dbAddrs), nil
}
