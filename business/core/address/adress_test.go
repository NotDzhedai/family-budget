package address_test

import (
	"context"
	"errors"
	"fmt"
	"testing"
	"time"

	"github.com/google/go-cmp/cmp"
	"gitlab.com/NotDzhedai/family-budget/business/core/address"
	"gitlab.com/NotDzhedai/family-budget/business/core/user"
	"gitlab.com/NotDzhedai/family-budget/business/data/dbschema"
	"gitlab.com/NotDzhedai/family-budget/business/data/dbtests"
	"gitlab.com/NotDzhedai/family-budget/foundation/docker"
)

var c *docker.Container

func TestMain(m *testing.M) {
	var err error
	c, err = dbtests.StartDB()
	if err != nil {
		fmt.Println(err)
		return
	}
	defer dbtests.StopDB(c)

	m.Run()
}

const migrationPath = "file://../../data/dbschema/sql/migrations"

func TestAddress(t *testing.T) {
	log, db, teardown := dbtests.NewUnit(t, c, "testaddress", migrationPath)
	t.Cleanup(teardown)

	userCore := user.NewCore(log, db)
	addressCore := address.NewCore(log, db)

	t.Log("Given the need to work with address records")
	{
		// prepare for test addresses
		t.Log("Create user")
		ctx := context.Background()
		now := time.Date(2018, time.October, 1, 0, 0, 0, 0, time.UTC)

		nu := user.NewUser{
			Name:            "Test User",
			Email:           "test@gmail.com",
			Password:        "qwerty",
			PasswordConfirm: "qwerty",
		}

		// already tested in users.
		// need for creating address
		usr, _ := userCore.Create(ctx, nu, now)

		// test create address
		const AddressName = "Test Address"
		na := address.NewAddress{
			Address: AddressName,
		}

		addr, err := addressCore.Create(ctx, usr.ID, na, now)
		if err != nil {
			t.Fatalf("\t%s\tTest:\tShould be able to create address : %s.", dbtests.Failed, err)
		}
		t.Logf("\t%s\tTest:\tShould be able to create address.", dbtests.Success)

		// test get address by id
		addrByID, err := addressCore.QueryByID(ctx, addr.ID, addr.UserID)
		if err != nil {
			t.Fatalf("\t%s\tTest:\tShould be able to get address by id: %s.", dbtests.Failed, err)
		}
		t.Logf("\t%s\tTest:\tShould be able to get address by id", dbtests.Success)

		if diff := cmp.Diff(addr, addrByID); diff != "" {
			t.Fatalf("\t%s\tTest:\tShould get back the same address: %s.", dbtests.Failed, err)
		}
		t.Logf("\t%s\tTest:\tShould get back the same address", dbtests.Success)

		// test update address
		ua := address.UpdateAddress{
			Address: "Updated Address",
		}
		if err = addressCore.Update(ctx, addr.ID, usr.ID, ua, now); err != nil {
			t.Fatalf("\t%s\tTest:\tShould be able to update address : %s.", dbtests.Failed, err)
		}

		// test get address be address (address varchar in table)
		addrByAddress, err := addressCore.QueryByAddress(ctx, ua.Address, usr.ID)
		if err != nil {
			t.Fatalf("\t%s\tTest:\tShould be able to get address by address: %s.", dbtests.Failed, err)
		}
		t.Logf("\t%s\tTest:\tShould be able to get address by address", dbtests.Success)

		if addrByAddress.Address != ua.Address {
			t.Errorf("\t%s\tTest:\tShold be able to see updates to Address", dbtests.Failed)
			t.Logf("\t\tTest:\tGot: %v", addrByAddress.Address)
			t.Logf("\t\tTest:\tExp: %v", ua.Address)
		} else {
			t.Logf("\t%s\tTest:\tShold be able to see updates to Address", dbtests.Success)
		}

		// test delete address
		if err := addressCore.Delete(ctx, addr.ID, addr.UserID); err != nil {
			t.Fatalf("\t%s\tTest:\tShould be able to delete address : %s.", dbtests.Failed, err)
		}
		t.Logf("\t%s\tTest:\tShould be able to delete address", dbtests.Success)

		_, err = addressCore.QueryByID(ctx, addr.ID, addr.UserID)
		if !errors.Is(err, address.ErrNotFound) {
			t.Fatalf("\t%s\tTest:\tShould NOT be able to retrieve address : %s.", dbtests.Failed, err)
		}
		t.Logf("\t%s\tTest:\tShould NOT be able to retrieve address", dbtests.Success)

	}
}

func TestPagingAddress(t *testing.T) {
	log, db, teardown := dbtests.NewUnit(t, c, "testpaging_address", migrationPath)
	t.Cleanup(teardown)

	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	dbschema.Seed(ctx, db)

	const UserID = "5cf37266-3473-4006-984f-9325122678b7"

	addressCore := address.NewCore(log, db)

	t.Log("Given the need to page through address records.")
	{
		address1, err := addressCore.Query(ctx, UserID, 1, 1)
		if err != nil {
			t.Fatalf("\t%s\tTest:\tShould be able to retrieve addresses for page 1 : %s.", dbtests.Failed, err)
		}
		t.Logf("\t%s\tTest:\tShould be able to retrieve addresses for page 1.", dbtests.Success)

		if len(address1) != 1 {
			t.Fatalf("\t%s\tTest:\tShould have a single user : %s.", dbtests.Failed, err)
		}
		t.Logf("\t%s\tTest:\tShould have a single user.", dbtests.Success)

		address2, err := addressCore.Query(ctx, UserID, 2, 1)
		if err != nil {
			t.Fatalf("\t%s\tTest:\tShould be able to retrieve addresses for page 2 : %s.", dbtests.Failed, err)
		}
		t.Logf("\t%s\tTest:\tShould be able to retrieve addresses for page 2.", dbtests.Success)

		if len(address2) != 1 {
			t.Fatalf("\t%s\tTest:\tShould have a single user : %s.", dbtests.Failed, err)
		}
		t.Logf("\t%s\tTest:\tShould have a single user.", dbtests.Success)

		if address1[0].ID == address2[0].ID {
			t.Logf("\t\tTest:\tPayment1: %v", address1[0].ID)
			t.Logf("\t\tTest:\tPayment2: %v", address2[0].ID)
			t.Fatalf("\t%s\tTest:\tShould have different addresses : %s.", dbtests.Failed, err)
		}
		t.Logf("\t%s\tTest:\tShould have different addresses.", dbtests.Success)
	}
}
