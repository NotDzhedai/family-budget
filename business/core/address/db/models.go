package db

import "time"

// Address represent the structure we need for moving data
// between the app and the database.
type Address struct {
	ID          string    `db:"address_id"`
	UserID      string    `db:"user_id"`
	Address     string    `db:"address"`
	DateCreated time.Time `db:"date_created"`
	DateUpdated time.Time `db:"date_updated"`
}
