// Package db contains user related CRUD functionality.
package db

import (
	"context"
	"fmt"

	"github.com/jmoiron/sqlx"
	"gitlab.com/NotDzhedai/family-budget/business/sys/database"
	"go.uber.org/zap"
)

// Store manages the set of API's for user access.
type Store struct {
	log          *zap.SugaredLogger
	tr           database.Transactor
	dbctx        sqlx.ExtContext
	isWithinTran bool
}

// NewStore constructs a data for api access.
func NewStore(log *zap.SugaredLogger, db *sqlx.DB) Store {
	return Store{
		log:   log,
		dbctx: db,
		tr:    db,
	}
}

// WithinTran runs passed function and do commit/rollback at the end.
func (s Store) WithinTran(ctx context.Context, fn func(sqlx.ExtContext) error) error {
	if s.isWithinTran {
		return fn(s.dbctx)
	}
	return database.WithinTran(ctx, s.log, s.tr, fn)
}

// Tran return new Store with transaction in it.
func (s Store) Tran(tx sqlx.ExtContext) Store {
	return Store{
		log:          s.log,
		tr:           s.tr,
		dbctx:        tx,
		isWithinTran: true,
	}
}

// Create inserts a new address into the database.
func (s Store) Create(ctx context.Context, addr Address) error {
	const q = `
	INSERT INTO addresses
		(address_id, user_id, address, date_created, date_updated)
	VALUES
		(:address_id, :user_id, :address, :date_created, :date_updated)
	`

	if err := database.NamedExecContext(ctx, s.log, s.dbctx, q, addr); err != nil {
		return fmt.Errorf("inserting address: %w", err)
	}

	return nil
}

// QueryByID gets the specified address from the database.
func (s Store) QueryByID(ctx context.Context, addrID, userID string) (Address, error) {
	data := struct {
		AddressID string `db:"address_id"`
		UserID    string `db:"user_id"`
	}{
		AddressID: addrID,
		UserID:    userID,
	}

	const q = `
	SELECT
		*
	FROM
		addresses
	WHERE 
		address_id = :address_id AND user_id = :user_id`

	var addr Address
	if err := database.NamedQueryStruct(ctx, s.log, s.dbctx, q, data, &addr); err != nil {
		return Address{}, fmt.Errorf("selecting addressID[%q]: %w", addrID, err)
	}

	return addr, nil
}

// QueryByAddress gets the specified address from the database by address.
func (s Store) QueryByAddress(ctx context.Context, addr, userID string) (Address, error) {

	data := struct {
		Address string `db:"address"`
		UserID  string `db:"user_id"`
	}{
		Address: addr,
		UserID:  userID,
	}

	const q = `
	SELECT
		*
	FROM
		addresses
	WHERE
		address = :address AND user_id = :user_id`

	var address Address
	if err := database.NamedQueryStruct(ctx, s.log, s.dbctx, q, data, &address); err != nil {
		return Address{}, fmt.Errorf("selecting address[%q]: %w", addr, err)
	}

	return address, nil
}

// Delete removes a address from the database.
func (s Store) Delete(ctx context.Context, addrID, userID string) error {
	data := struct {
		AddressID string `db:"address_id"`
		UserID    string `db:"user_id"`
	}{
		AddressID: addrID,
		UserID:    userID,
	}

	const q = `
	DELETE FROM 
		addresses
	WHERE
		address_id = :address_id AND user_id = :user_id
	`
	if err := database.NamedExecContext(ctx, s.log, s.dbctx, q, data); err != nil {
		return fmt.Errorf("deleting addressID[%s]: %w", addrID, err)
	}

	return nil
}

// Update replaces a address document in the database.
func (s Store) Update(ctx context.Context, addr Address) error {
	const q = `
	UPDATE
		addresses
	SET
		"address" = :address,
		"date_updated" = :date_updated
	WHERE
		address_id = :address_id AND user_id = :user_id
	`

	if err := database.NamedExecContext(ctx, s.log, s.dbctx, q, addr); err != nil {
		return fmt.Errorf("updating userID[%s]: %w", addr.ID, err)
	}

	return nil
}

// Query retrieves a list of existing addresses from the database.
func (s Store) Query(ctx context.Context, userID string, pageNumber int, rowsPerPage int) ([]Address, error) {
	data := struct {
		Offset      int    `db:"offset"`
		RowsPerPage int    `db:"rows_per_page"`
		UserID      string `db:"user_id"`
	}{
		Offset:      (pageNumber - 1) * rowsPerPage,
		RowsPerPage: rowsPerPage,
		UserID:      userID,
	}

	const q = `
	SELECT 
		*
	FROM
		addresses
	WHERE
		user_id = :user_id
	ORDER BY 
		date_created
	OFFSET :offset ROWS FETCH NEXT :rows_per_page ROWS ONLY
	`

	var addrs []Address
	if err := database.NamedQuerySlice(ctx, s.log, s.dbctx, q, data, &addrs); err != nil {
		return nil, fmt.Errorf("selecting users: %w", err)
	}

	return addrs, nil
}
