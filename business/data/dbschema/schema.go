// Package dbschema contains the database schema, migrations and seeding data.
package dbschema

import (
	"context"
	_ "embed"
	"fmt"

	"github.com/golang-migrate/migrate/v4"
	_ "github.com/golang-migrate/migrate/v4/database/postgres"
	_ "github.com/golang-migrate/migrate/v4/source/file"
	"github.com/jmoiron/sqlx"
	"gitlab.com/NotDzhedai/family-budget/business/sys/database"
)

var (
	//go:embed sql/seed.sql
	seedDoc string
)

// MigrateUP attempts to bring the schema for db up to date with the migrations
// defined in this package. UP
func MigrateUP(ctx context.Context, db *sqlx.DB, dbString string, pathToMigrations string) error {
	if err := database.StatusCheck(ctx, db); err != nil {
		return fmt.Errorf("status check database: %w", err)
	}

	m, err := migrate.New(pathToMigrations, dbString)
	if err != nil {
		return err
	}

	return m.Up()
}

// MigrateDown attempts to bring the schema for db up to date with the migrations
// defined in this package. DOWN
func MigrateDown(ctx context.Context, db *sqlx.DB, dbString string, pathToMigrations string) error {
	if err := database.StatusCheck(ctx, db); err != nil {
		return fmt.Errorf("status check database: %w", err)
	}

	m, err := migrate.New(pathToMigrations, dbString)
	if err != nil {
		return err
	}

	return m.Down()
}

// Seed runs the set of seed-data queries against db. The queries are ran in a
// transaction and rolled back if any fail.
func Seed(ctx context.Context, db *sqlx.DB) error {
	if err := database.StatusCheck(ctx, db); err != nil {
		return fmt.Errorf("status check database: %w", err)
	}

	tx, err := db.Begin()
	if err != nil {
		return err
	}

	if _, err := tx.Exec(seedDoc); err != nil {
		if err := tx.Rollback(); err != nil {
			return err
		}
		return err
	}

	return tx.Commit()
}
