CREATE TABLE addresses (
  address_id VARCHAR PRIMARY KEY,
  user_id VARCHAR REFERENCES users (user_id) ON DELETE CASCADE,
  address VARCHAR NOT NULL,
  date_created TIMESTAMP NOT NULL,
	date_updated TIMESTAMP NOT NULL
);

CREATE UNIQUE INDEX idx_user_id_address ON addresses(user_id, address);