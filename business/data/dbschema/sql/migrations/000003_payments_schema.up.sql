CREATE TABLE utility_payments (
  payment_id VARCHAR PRIMARY KEY,
  user_id VARCHAR REFERENCES users(user_id) ON DELETE CASCADE,
  address_id VARCHAR REFERENCES addresses(address_id) ON DELETE CASCADE,
  date_created TIMESTAMP NOT NULL,
  gas NUMERIC NOT NULL,
  gas_transit NUMERIC NOT NULL,
  light NUMERIC NOT NULL,
  water NUMERIC NOT NULL,
  waste NUMERIC NOT NULL,
  apartment_fee NUMERIC NOT NULL,
  elevator NUMERIC NOT NULL,
  intercom NUMERIC NOT NULL,
  heating_fee NUMERIC NOT NULL,
  heating_subscription_fee NUMERIC NOT NULL
);
