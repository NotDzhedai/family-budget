// This program takes the structured log output and makes it readable.
package main

import (
	"bufio"
	"encoding/json"
	"flag"
	"fmt"
	"log"
	"os"
	"strings"
)

var (
	Reset  = "\033[0m"
	Red    = "\033[31m"
	Green  = "\033[32m"
	Yellow = "\033[33m"
	Blue   = "\033[34m"
	Purple = "\033[35m"
	Cyan   = "\033[36m"
	Gray   = "\033[37m"
	White  = "\033[97m"
)

var service string

func init() {
	flag.StringVar(&service, "service", "", "filter which service to see")
}

func main() {
	flag.Parse()
	var b strings.Builder

	// Scan standard input for log data per line.
	scanner := bufio.NewScanner(os.Stdin)
	for scanner.Scan() {
		s := scanner.Text()

		// Convert the JSON to a map for processing.
		m := make(map[string]any)
		err := json.Unmarshal([]byte(s), &m)
		if err != nil {
			if service == "" {
				fmt.Println(s)
			}
			continue
		}

		// If a service filter was provided, check.
		if service != "" && m["service"] != service {
			continue
		}

		// traceid
		traceID := "00000000-0000-0000-0000-000000000000"
		if v, ok := m["traceid"]; ok {
			traceID = fmt.Sprintf("%v", v)
		}

		// Build out the know portions of the log in the order
		// I want them in.
		b.Reset()
		b.WriteString(fmt.Sprintf("%s %s: %s: %s: %s: %s: ",
			Reset,
			m["service"],
			m["ts"],
			m["level"],
			traceID,
			m["caller"],
		))

		b.WriteString(fmt.Sprintf("%s %v %s ", Gray, strings.ToUpper(m["msg"].(string)), Reset))

		status, ok := m["status"]
		if ok {
			b.WriteString(fmt.Sprintf("%s %v %s %v ", Cyan, "status", Reset, status))
		}

		host, ok := m["host"]
		if ok {
			b.WriteString(fmt.Sprintf("%s %v %s %v ", Cyan, "host", Reset, host))
		}
		method, ok := m["method"]
		if ok {
			b.WriteString(fmt.Sprintf("%s %v %v %s ", Cyan, "method", colorMethod(method.(string)), Reset))
		}
		statusCode, ok := m["statuscode"]
		if ok {
			// ?  Каст в float64. Хз чому воно так хоче... По іншому не працює
			b.WriteString(fmt.Sprintf("%s %v %v %s ", Cyan, "statuscode", colorStatus(statusCode.(float64)), Reset))
		}
		path, ok := m["path"]
		if ok {
			b.WriteString(fmt.Sprintf("%s %v %v %s ", Gray, "path", path, Reset))
		}

		// Add the rest of the keys ignoring the ones we already
		// added for the log.
		for k, v := range m {
			switch k {
			case "service", "ts", "level", "traceid", "caller", "msg", "status", "host", "method", "statuscode":
				continue
			}

			// It's nice to see the key[value] in this format
			// especially since map ordering is random.
			b.WriteString(fmt.Sprintf("%s %s: %v: ", Reset, k, v))
		}

		// Write the new log format, removing the last :
		out := b.String()
		fmt.Println(out[:len(out)-1])
	}

	if err := scanner.Err(); err != nil {
		log.Println(err)
	}
}

func colorMethod(method string) string {
	switch method {
	case "GET":
		return fmt.Sprintf("%s %s", Green, method)
	case "POST":
		return fmt.Sprintf("%s %s", Yellow, method)
	case "DELETE":
		return fmt.Sprintf("%s %s", Red, method)
	case "PUT":
		return fmt.Sprintf("%s %s", Blue, method)
	default:
		return method
	}
}

func colorStatus(status float64) string {
	switch status {
	case 200, 204:
		return fmt.Sprintf("%s %.0f", Green, status)
	default:
		return fmt.Sprintf("%s %.0f", Red, status)
	}
}
