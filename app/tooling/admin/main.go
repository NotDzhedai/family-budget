package main

import (
	"errors"
	"fmt"
	"os"

	"github.com/ardanlabs/conf/v3"
	"gitlab.com/NotDzhedai/family-budget/app/tooling/admin/commands"
	"gitlab.com/NotDzhedai/family-budget/business/sys/database"
	"gitlab.com/NotDzhedai/family-budget/foundation/logger"
	"go.uber.org/zap"
)

var build = "develop"

func main() {

	// Construct the application logger.
	log, err := logger.New("ADMIN")
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
	defer log.Sync()

	if err := run(log); err != nil {
		log.Errorw("startup admin", "ERROR", err)
		log.Sync()
		os.Exit(1)
	}
}

func run(log *zap.SugaredLogger) error {
	// =========================================================================
	// Configuration

	cfg := struct {
		conf.Version
		Args conf.Args
		DB   struct {
			User       string `conf:"default:postgres"`
			Password   string `conf:"default:postgres,mask"`
			Host       string `conf:"default:localhost"`
			Name       string `conf:"default:postgres"`
			DisableTLS bool   `conf:"default:true"`
		}
	}{
		Version: conf.Version{
			Build: build,
			Desc:  "ndz family budget admin",
		},
	}

	const prefix = "fb admin"
	help, err := conf.Parse(prefix, &cfg)
	if err != nil {
		if errors.Is(err, conf.ErrHelpWanted) {
			fmt.Println(help)
			return nil
		}
		return fmt.Errorf("parsing config: %w", err)
	}

	// out
	_, err = conf.String(&cfg)
	if err != nil {
		return fmt.Errorf("generating config for output: %w", err)
	}
	// log.Infow("startup admin", "config", out)

	// =========================================================================
	// Commands

	dbConfig := database.Config{
		User:       cfg.DB.User,
		Password:   cfg.DB.Password,
		Host:       cfg.DB.Host,
		Name:       cfg.DB.Name,
		DisableTLS: cfg.DB.DisableTLS,
	}

	return processCommands(cfg.Args, log, dbConfig)
}

func processCommands(args conf.Args, log *zap.SugaredLogger, dbConfig database.Config) error {
	switch args.Num(0) {
	// commands processing
	case "migrateUp":
		if err := commands.MigrateUP(dbConfig); err != nil {
			return fmt.Errorf("migrating database: %w", err)
		}
	case "migrateDown":
		if err := commands.MigrateDOWN(dbConfig); err != nil {
			return fmt.Errorf("migrating database: %w", err)
		}
	case "seed":
		if err := commands.Seed(dbConfig); err != nil {
			return fmt.Errorf("seeding database: %w", err)
		}
	case "genKey":
		if err := commands.GenKey(); err != nil {
			return fmt.Errorf("key generation: %w", err)
		}
	case "genToken":
		userID := args.Num(1)
		kid := args.Num(2)
		if err := commands.GenToken(log, dbConfig, userID, kid); err != nil {
			return fmt.Errorf("generating token: %w", err)
		}
	}

	return nil
}
