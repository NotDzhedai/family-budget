package commands

import (
	"context"
	"errors"
	"fmt"
	"time"

	"github.com/golang-migrate/migrate/v4"
	"gitlab.com/NotDzhedai/family-budget/business/data/dbschema"
	"gitlab.com/NotDzhedai/family-budget/business/sys/database"
)

const migrationsPath = "file://migrations"

func MigrateUP(cfg database.Config) error {
	db, err := database.Open(cfg)
	if err != nil {
		return fmt.Errorf("connect database: %w", err)
	}
	defer db.Close()

	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	if err := dbschema.MigrateUP(ctx, db, cfg.GetDBString(), migrationsPath); err != nil {
		if !errors.Is(err, migrate.ErrNoChange) {
			return fmt.Errorf("migrate database: %w", err)
		}
	}

	fmt.Println("migrations complete")
	return nil
}

func MigrateDOWN(cfg database.Config) error {
	db, err := database.Open(cfg)
	if err != nil {
		return fmt.Errorf("connect database: %w", err)
	}
	defer db.Close()

	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	if err := dbschema.MigrateDown(ctx, db, cfg.GetDBString(), migrationsPath); err != nil {
		if !errors.Is(err, migrate.ErrNoChange) {
			return fmt.Errorf("migrate database: %w", err)
		}
	}

	fmt.Println("migrations complete")
	return nil
}
