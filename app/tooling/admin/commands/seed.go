package commands

import (
	"context"
	"fmt"
	"time"

	"gitlab.com/NotDzhedai/family-budget/business/data/dbschema"
	"gitlab.com/NotDzhedai/family-budget/business/sys/database"
)

func Seed(cfg database.Config) error {
	db, err := database.Open(cfg)
	if err != nil {
		return fmt.Errorf("connect database: %w", err)
	}
	defer db.Close()

	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	if err := dbschema.Seed(ctx, db); err != nil {
		return fmt.Errorf("seed database: %w", err)

	}

	fmt.Println("seed complete")
	return nil
}
