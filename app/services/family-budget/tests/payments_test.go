package tests

import (
	"bytes"
	"encoding/json"
	"fmt"
	"net/http"
	"net/http/httptest"
	"os"
	"strings"
	"testing"

	"github.com/google/go-cmp/cmp"
	"github.com/google/uuid"
	"gitlab.com/NotDzhedai/family-budget/app/services/family-budget/handlers"
	"gitlab.com/NotDzhedai/family-budget/business/core/payment"
	"gitlab.com/NotDzhedai/family-budget/business/data/dbtests"
)

type PaymentTests struct {
	app http.Handler
	// юзер у якого немає адрес та платежів в бд
	userWithoutData string
	// юзер у якого є парочка адрес та платежів в бд
	userWithData string
	// юзер у якого немає адрес та платежів в бд. Потрібен для тесту пустого слвйсу
	emptyUserToken string
}

func TestPayment(t *testing.T) {
	test := dbtests.NewIntegration(t, c, "inittestpayment", migrationPath)
	t.Cleanup(test.Teardown)

	shutdown := make(chan os.Signal, 1)
	tests := PaymentTests{
		app: handlers.APIMux(handlers.APIMuxConfig{
			Shutdown: shutdown,
			Log:      test.Log,
			Auth:     test.Auth,
			DB:       test.DB,
		}),
		userWithoutData: test.Token("user@example.com", "gophers"),
		userWithData:    test.Token("admin@example.com", "gophers"),
		emptyUserToken:  test.Token("empty@example.com", "gophers"),
	}

	// create
	t.Run("create201", tests.create201)
	t.Run("create401", tests.create401)
	t.Run("create400InvalidAddress", tests.create400InvalidAddress)
	t.Run("create400InvalidData", tests.create400InvalidData)
	// query by id
	t.Run("queryByID200", tests.queryByID200)
	t.Run("queryByID404", tests.queryByID404)
	t.Run("queryByID400", tests.queryByID400)
	// query by address
	t.Run("query200", tests.query200)
	t.Run("query200empty", tests.query200empty)
	t.Run("query400", tests.query400)
	t.Run("query400InvalidID", tests.query400InvalidID)
	// update
	t.Run("update204", tests.update204)
	t.Run("update401", tests.update401)
	t.Run("update400", tests.update400)
	t.Run("update404", tests.update404)
	t.Run("update400InvalidID", tests.update400InvalidID)
	// delete
	t.Run("delete204", tests.delete204)
	t.Run("delete400", tests.delete400)
	t.Run("delete404", tests.delete404)
	t.Run("delete401", tests.delete401)

}

// TESTS FOR CREATE
func (at *PaymentTests) create201(t *testing.T) {
	const AddressID = "76b983d2-8c49-112a-864d-2c950d33d23e"

	np := payment.NewPayment{
		AddressID:              AddressID,
		Gas:                    10,
		GasTransit:             10,
		Light:                  10,
		Water:                  10,
		Waste:                  10,
		ApartmentFee:           10,
		Elevator:               10,
		Intercom:               10,
		HeatingFee:             10,
		HeatingSubscriptionFee: 10,
	}

	body, err := json.Marshal(np)
	if err != nil {
		t.Fatal(err)
	}

	r := httptest.NewRequest(http.MethodPost, "/v1/payment", bytes.NewBuffer(body))
	w := httptest.NewRecorder()

	r.Header.Set("Authorization", "Bearer "+at.userWithData)
	at.app.ServeHTTP(w, r)

	{
		if w.Code != http.StatusCreated {
			t.Fatalf("\t%s\tTest:\tShould receive a status code of 201 for the response : %v", dbtests.Failed, w.Code)
		}
		t.Logf("\t%s\tTest:\tShould receive a status code of 201 for the response", dbtests.Failed)
	}
}

func (at *PaymentTests) create401(t *testing.T) {
	const AddressID = "76b983d2-8c49-112a-864d-2c950d33d23e"

	np := payment.NewPayment{
		AddressID:              AddressID,
		Gas:                    10,
		GasTransit:             10,
		Light:                  10,
		Water:                  10,
		Waste:                  10,
		ApartmentFee:           10,
		Elevator:               10,
		Intercom:               10,
		HeatingFee:             10,
		HeatingSubscriptionFee: 10,
	}

	body, err := json.Marshal(np)
	if err != nil {
		t.Fatal(err)
	}

	r := httptest.NewRequest(http.MethodPost, "/v1/payment", bytes.NewBuffer(body))
	w := httptest.NewRecorder()

	r.Header.Set("Authorization", "invalid token")
	at.app.ServeHTTP(w, r)

	{
		if w.Code != http.StatusUnauthorized {
			t.Fatalf("\t%s\tTest:\tShould receive a status code of 401 for the response : %v", dbtests.Failed, w.Code)
		}
		t.Logf("\t%s\tTest:\tShould receive a status code of 401 for the response", dbtests.Success)
	}
}

func (at *PaymentTests) create400InvalidAddress(t *testing.T) {
	np := payment.NewPayment{
		AddressID:              uuid.New().String(),
		Gas:                    10,
		GasTransit:             10,
		Light:                  10,
		Water:                  10,
		Waste:                  10,
		ApartmentFee:           10,
		Elevator:               10,
		Intercom:               10,
		HeatingFee:             10,
		HeatingSubscriptionFee: 10,
	}

	body, err := json.Marshal(np)
	if err != nil {
		t.Fatal(err)
	}

	r := httptest.NewRequest(http.MethodPost, "/v1/payment", bytes.NewBuffer(body))
	w := httptest.NewRecorder()

	r.Header.Set("Authorization", "Bearer "+at.userWithData)
	at.app.ServeHTTP(w, r)

	{
		if w.Code != http.StatusBadRequest {
			t.Fatalf("\t%s\tTest:\tShould receive a status code of 400 for the response : %v", dbtests.Failed, w.Code)
		}
		t.Logf("\t%s\tTest:\tShould receive a status code of 400 for the response", dbtests.Success)
	}
}

func (at *PaymentTests) create400InvalidData(t *testing.T) {
	const AddressID = "76b983d2-8c49-112a-864d-2c950d33d23e"

	np := payment.NewPayment{
		AddressID:              AddressID,
		Gas:                    -10, // invalid
		GasTransit:             10,
		Light:                  10,
		Water:                  10,
		Waste:                  10,
		ApartmentFee:           10,
		Elevator:               10,
		Intercom:               10,
		HeatingFee:             10,
		HeatingSubscriptionFee: 10,
	}

	body, err := json.Marshal(np)
	if err != nil {
		t.Fatal(err)
	}

	r := httptest.NewRequest(http.MethodPost, "/v1/payment", bytes.NewBuffer(body))
	w := httptest.NewRecorder()

	r.Header.Set("Authorization", "Bearer "+at.userWithData)
	at.app.ServeHTTP(w, r)

	{
		if w.Code != http.StatusBadRequest {
			t.Fatalf("\t%s\tTest:\tShould receive a status code of 400 for the response : %v", dbtests.Failed, w.Code)
		}
		t.Logf("\t%s\tTest:\tShould receive a status code of 400 for the response", dbtests.Success)
	}
}

// TESTS FOR QUERYBYID
func (at *PaymentTests) queryByID200(t *testing.T) {
	const PaymentID = "66721684-6ec4-490b-a575-ee470b2f3f67"

	r := httptest.NewRequest(http.MethodGet, fmt.Sprintf("/v1/payment/%s", PaymentID), nil)
	w := httptest.NewRecorder()

	// userWithData only!
	r.Header.Set("Authorization", "Bearer "+at.userWithData)
	at.app.ServeHTTP(w, r)

	t.Log("Given the need to get payment by id")
	{
		if w.Code != http.StatusOK {
			t.Fatalf("\t%s\tTest:\tShould receive a status code of 200 for the response : %v", dbtests.Failed, w.Code)
		}
		t.Logf("\t%s\tTest:\tShould receive a status code of 200 for the response", dbtests.Success)

		var got payment.Payment
		if err := json.NewDecoder(w.Body).Decode(&got); err != nil {
			t.Fatalf("\t%s\tTest:\tShould be able to unmarshal the response to an payment type : %v", dbtests.Failed, err)
		}
		t.Logf("\t%s\tTest:\tShould be able to unmarshal the response to an payment type.", dbtests.Success)

		pmt := payment.Payment{
			ID:                     PaymentID,
			AddressID:              "76b983d2-8c49-469c-864d-2c950d33d2d2",
			UserID:                 "5cf37266-3473-4006-984f-9325122678b7",
			DateCreated:            got.DateCreated, // rework
			Gas:                    11.1,
			GasTransit:             12.2,
			Light:                  13.3,
			Water:                  14.4,
			Waste:                  15.5,
			ApartmentFee:           110.10,
			Elevator:               120.20,
			Intercom:               130.30,
			HeatingFee:             140.40,
			HeatingSubscriptionFee: 1234.1234,
		}

		if diff := cmp.Diff(got, pmt, nil); diff != "" {
			t.Fatalf("\t%s\tTest:\tShould get the expected result. Diff:\n%s", dbtests.Failed, diff)
		}
		t.Logf("\t%s\tTest:\tShould get the expected result.", dbtests.Success)
	}
}

func (at *PaymentTests) queryByID404(t *testing.T) {
	const PaymentID = "66721684-6ec4-490b-a575-ee470b2f3f67"

	r := httptest.NewRequest(http.MethodGet, fmt.Sprintf("/v1/payment/%s", PaymentID), nil)
	w := httptest.NewRecorder()

	// Address belongs to userWithData. With userWithoutData -> 404
	r.Header.Set("Authorization", "Bearer "+at.userWithoutData)
	at.app.ServeHTTP(w, r)

	t.Log("Given the need to get payment by id")
	{
		if w.Code != http.StatusNotFound {
			t.Fatalf("\t%s\tTest:\tShould receive a status code of 404 for the response : %v", dbtests.Failed, w.Code)
		}
		t.Logf("\t%s\tTest:\tShould receive a status code of 404 for the response", dbtests.Success)
	}
}

func (at *PaymentTests) queryByID400(t *testing.T) {
	const InvalidPaymentID = "76b983d2-8c49-469c-864d-2c950d33d2"
	r := httptest.NewRequest(http.MethodGet, fmt.Sprintf("/v1/payment/%s", InvalidPaymentID), nil)
	w := httptest.NewRecorder()

	r.Header.Set("Authorization", "Bearer "+at.userWithData)
	at.app.ServeHTTP(w, r)

	t.Log("Given the need to get payment by id")
	{
		if w.Code != http.StatusBadRequest {
			t.Fatalf("\t%s\tTest:\tShould receive a status code of 400 for the response : %v", dbtests.Failed, w.Code)
		}
		t.Logf("\t%s\tTest:\tShould receive a status code of 400 for the response", dbtests.Success)
	}
}

// TESTS FOR QUERYBYADDRESS
func (at *PaymentTests) query200(t *testing.T) {
	const AddressID = "76b983d2-8c49-469c-864d-2c950d33d2d2"

	r := httptest.NewRequest(http.MethodGet, fmt.Sprintf("/v1/payments/%s/%d/%d", AddressID, 1, 2), nil)
	w := httptest.NewRecorder()

	r.Header.Set("Authorization", "Bearer "+at.userWithData)
	at.app.ServeHTTP(w, r)

	t.Log("Given the need to query payments")
	{
		if w.Code != http.StatusOK {
			t.Fatalf("\t%s\tTest:\tShould receive a status code of 200 for the response : %v", dbtests.Failed, w.Code)
		}
		t.Logf("\t%s\tTest:\tShould receive a status code of 200 for the response", dbtests.Success)

		var got struct {
			Payments []payment.Payment `json:"payments"`
			Pages    int               `json:"pages"`
		}
		if err := json.NewDecoder(w.Body).Decode(&got); err != nil {
			t.Fatalf("\t%s\tTest:\tShould be able to unmarshal the response to an payment type : %v", dbtests.Failed, err)
		}
		t.Logf("\t%s\tTest:\tShould be able to unmarshal the response to an payment type", dbtests.Success)

		t.Log(got)

		if len(got.Payments) != 2 {
			t.Fatalf("\t%s\tTest:\tShould get the len of 2. Got:%d", dbtests.Failed, len(got.Payments))
		}
		t.Logf("\t%s\tTest:\tShould get the len of 2. Got:%d", dbtests.Success, len(got.Payments))

		// some checks
		if got.Payments[0].Light != 13.3 {
			t.Logf("\tTest:\tGot: %f", got.Payments[0].Light)
			t.Logf("\tTest:\tExp: %f", 13.3)
			t.Errorf("\t%s\tTest:\tShould be the same payment light", dbtests.Failed)
		}
		t.Logf("\t%s\tTest:\tShould be the same payment light", dbtests.Success)

		if got.Payments[1].HeatingSubscriptionFee != 1234.1234 {
			t.Logf("\tTest:\tGot: %f", got.Payments[1].HeatingSubscriptionFee)
			t.Logf("\tTest:\tExp: %f", 1234.1234)
			t.Errorf("\t%s\tTest:\tShould be the same payment heating_subscription_fee", dbtests.Failed)
		}
		t.Logf("\t%s\tTest:\tShould be the same payment heating_subscription_fee", dbtests.Success)

		if got.Pages != 6 {
			t.Logf("\tTest:\tGot: %d", got.Pages)
			t.Logf("\tTest:\tExp: %d", 1)
			t.Errorf("\t%s\tTest:\tShould be able to get len of 2", dbtests.Failed)
		}
		t.Logf("\t%s\tTest:\tShould be able to get len of 2", dbtests.Success)

	}
}

// Коли у юзера немає платежів. Повинен повернутись пустий слайс
func (at *PaymentTests) query200empty(t *testing.T) {
	const AddressID = "76b983d2-8c49-469c-864d-2c950d33d2d2"

	r := httptest.NewRequest(http.MethodGet, fmt.Sprintf("/v1/payments/%s/%d/%d", AddressID, 1, 2), nil)
	w := httptest.NewRecorder()

	r.Header.Set("Authorization", "Bearer "+at.emptyUserToken)
	at.app.ServeHTTP(w, r)

	t.Log("Given the need to query addresses")
	{
		if w.Code != http.StatusOK {
			t.Fatalf("\t%s\tTest:\tShould receive a status code of 200 for the response : %v", dbtests.Failed, w.Code)
		}
		t.Logf("\t%s\tTest:\tShould receive a status code of 200 for the response", dbtests.Success)

		var got struct {
			Payments []payment.Payment `json:"payments"`
			Pages    int               `json:"pages"`
		}
		if err := json.NewDecoder(w.Body).Decode(&got); err != nil {
			t.Fatalf("\t%s\tTest:\tShould be able to unmarshal the response to an payment type : %v", dbtests.Failed, err)
		}
		t.Logf("\t%s\tTest:\tShould be able to unmarshal the response to an payment type", dbtests.Success)

		if len(got.Payments) != 0 {
			t.Fatalf("\t%s\tTest:\tShould get the len of 0. Got:%d", dbtests.Failed, len(got.Payments))
		}
		t.Logf("\t%s\tTest:\tShould get the len of 0. Got:%d", dbtests.Success, len(got.Payments))

		if got.Pages != 0 {
			t.Fatalf("\t%s\tTest:\tShould get pages of 0. Got:%d", dbtests.Failed, got.Pages)
		}
		t.Logf("\t%s\tTest:\tShould get pages of 0. Got:%d", dbtests.Success, got.Pages)
	}
}

// Передача параметрів неправильного типу
func (at *PaymentTests) query400(t *testing.T) {
	r := httptest.NewRequest(http.MethodGet, fmt.Sprintf("/v1/payments/%s/%f/%d", "invalid", 1.3, 2), nil) // wrong param type
	w := httptest.NewRecorder()

	r.Header.Set("Authorization", "Bearer "+at.userWithData)
	at.app.ServeHTTP(w, r)

	t.Log("Given the need to query payments]")
	{
		if w.Code != http.StatusBadRequest {
			t.Fatalf("\t%s\tTest:\tShould receive a status code of 400 for the response : %v", dbtests.Failed, w.Code)
		}
		t.Logf("\t%s\tTest:\tShould receive a status code of 400 for the response", dbtests.Success)
	}
}

func (at *PaymentTests) query400InvalidID(t *testing.T) {

	r := httptest.NewRequest(http.MethodGet, fmt.Sprintf("/v1/payments/%s/%d/%d", "invalid", 1, 2), nil)
	w := httptest.NewRecorder()

	r.Header.Set("Authorization", "Bearer "+at.userWithData)
	at.app.ServeHTTP(w, r)

	t.Log("Given the need to query payments]")
	{
		if w.Code != http.StatusBadRequest {
			t.Fatalf("\t%s\tTest:\tShould receive a status code of 400 for the response : %v", dbtests.Failed, w.Code)
		}
		t.Logf("\t%s\tTest:\tShould receive a status code of 400 for the response", dbtests.Success)
	}
}

// TEST FOR DELETE
func (at *PaymentTests) delete204(t *testing.T) {
	const PaymentID = "ac22dc44-1778-4708-8d0e-f001c2ee0fbf"
	r := httptest.NewRequest(http.MethodDelete, fmt.Sprintf("/v1/payment/%s", PaymentID), nil)
	w := httptest.NewRecorder()

	r.Header.Set("Authorization", "Bearer "+at.userWithData)
	at.app.ServeHTTP(w, r)

	t.Log("Given the need to delete payment")
	{
		if w.Code != http.StatusNoContent {
			t.Fatalf("\t%s\tTest:\tShould receive a status code of 204 for the response : %v", dbtests.Failed, w.Code)
		}
		t.Logf("\t%s\tTest:\tShould receive a status code of 204 for the response", dbtests.Success)
	}

	// check id address deleted
	t.Log("Check if deleted")
	r = httptest.NewRequest(http.MethodGet, fmt.Sprintf("/v1/payment/%s", PaymentID), nil)
	w = httptest.NewRecorder()

	r.Header.Set("Authorization", "Bearer "+at.userWithData)
	at.app.ServeHTTP(w, r)

	if w.Code == http.StatusNotFound {
		t.Logf("\t%s\tTest:\tShould receive a status code of 404 for the response", dbtests.Success)
	}
}

func (at *PaymentTests) delete400(t *testing.T) {
	const InvalidPaymentID = "invalidID"

	r := httptest.NewRequest(http.MethodDelete, fmt.Sprintf("/v1/payment/%s", InvalidPaymentID), nil)
	w := httptest.NewRecorder()

	r.Header.Set("Authorization", "Bearer "+at.userWithData)
	at.app.ServeHTTP(w, r)

	t.Log("Given the need to delete payment")
	{
		if w.Code != http.StatusBadRequest {
			t.Fatalf("\t%s\tTest:\tShould receive a status code of 400 for the response : %v", dbtests.Failed, w.Code)
		}
		t.Logf("\t%s\tTest:\tShould receive a status code of 400 for the response", dbtests.Success)
	}
}

func (at *PaymentTests) delete404(t *testing.T) {
	randomUUID := uuid.New()

	r := httptest.NewRequest(http.MethodDelete, fmt.Sprintf("/v1/payment/%s", randomUUID), nil)
	w := httptest.NewRecorder()

	r.Header.Set("Authorization", "Bearer "+at.userWithData)
	at.app.ServeHTTP(w, r)

	t.Log("Given the need to delete payment")
	{
		if w.Code != http.StatusNotFound {
			t.Fatalf("\t%s\tTest:\tShould receive a status code of 404 for the response : %v", dbtests.Failed, w.Code)
		}
		t.Logf("\t%s\tTest:\tShould receive a status code of 404 for the response", dbtests.Success)
	}
}

func (at *PaymentTests) delete401(t *testing.T) {
	const PaymentID = "ac22dc44-1778-4708-8d0e-f001c2ee0fbf"

	r := httptest.NewRequest(http.MethodDelete, fmt.Sprintf("/v1/payment/%s", PaymentID), nil)
	w := httptest.NewRecorder()

	r.Header.Set("Authorization", "invalid token")
	at.app.ServeHTTP(w, r)

	t.Log("Given the need to delete payment")
	{
		if w.Code != http.StatusUnauthorized {
			t.Fatalf("\t%s\tTest:\tShould receive a status code of 401 for the response : %v", dbtests.Failed, w.Code)
		}
		t.Logf("\t%s\tTest:\tShould receive a status code of 401 for the response", dbtests.Success)
	}
}

// TESTS FOR UPDATE
func (at *PaymentTests) update204(t *testing.T) {
	const PaymentID = "ac22dc44-1778-4708-8d0e-f001c2ee0fbf"

	ua := payment.UpdatePayment{
		Gas:        dbtests.FloatPointer(1000),
		GasTransit: dbtests.FloatPointer(2000),
		Light:      dbtests.FloatPointer(3000),
	}

	body, err := json.Marshal(ua)
	if err != nil {
		t.Fatal(err)
	}

	r := httptest.NewRequest(http.MethodPut, fmt.Sprintf("/v1/payment/%s", PaymentID), bytes.NewBuffer(body))
	w := httptest.NewRecorder()

	r.Header.Set("Authorization", "Bearer "+at.userWithData)
	at.app.ServeHTTP(w, r)

	t.Log("Given the need to update payment")
	{
		if w.Code != http.StatusNoContent {
			t.Fatalf("\t%s\tTest:\tShould receive a status code of 204 for the response : %v", dbtests.Failed, w.Code)
		}
		t.Logf("\t%s\tTest:\tShould receive a status code of 204 for the response", dbtests.Success)
	}

	t.Log("Check if payment updated")
	{
		r = httptest.NewRequest(http.MethodGet, fmt.Sprintf("/v1/payment/%s", PaymentID), nil)
		w = httptest.NewRecorder()

		r.Header.Set("Authorization", "Bearer "+at.userWithData)
		at.app.ServeHTTP(w, r)

		var got payment.Payment
		if err := json.NewDecoder(w.Body).Decode(&got); err != nil {
			t.Fatalf("\t%s\tTest:\tShould be able to unmarshal the response to an payment type : %v", dbtests.Failed, err)
		}
		t.Logf("\t%s\tTest:\tShould be able to unmarshal the response to an payment type.", dbtests.Success)

		exp := payment.Payment{
			ID:                     PaymentID,
			AddressID:              "76b983d2-8c49-469c-864d-2c950d33d2d2",
			UserID:                 "5cf37266-3473-4006-984f-9325122678b7",
			DateCreated:            got.DateCreated, // rework
			Gas:                    1000,            // changed
			GasTransit:             2000,            // changed
			Light:                  3000,            // changed
			Water:                  14.4,
			Waste:                  15.5,
			ApartmentFee:           110.10,
			Elevator:               120.20,
			Intercom:               130.30,
			HeatingFee:             140.40,
			HeatingSubscriptionFee: 1234.1234,
		}

		if diff := cmp.Diff(got, exp, nil); diff != "" {
			t.Fatalf("\t%s\tTest:\tShould get the expected result. Diff:\n%s", dbtests.Failed, diff)
		}
		t.Logf("\t%s\tTest:\tShould get the expected result.", dbtests.Success)
	}
}

func (at *PaymentTests) update401(t *testing.T) {
	const PaymentID = "ac22dc44-1778-4708-8d0e-f001c2ee0fbf"

	r := httptest.NewRequest(http.MethodPut, fmt.Sprintf("/v1/payment/%s", PaymentID), nil)
	w := httptest.NewRecorder()

	r.Header.Set("Authorization", "invalid token")
	at.app.ServeHTTP(w, r)

	t.Log("Given the need to update payment")
	{
		if w.Code != http.StatusUnauthorized {
			t.Fatalf("\t%s\tTest:\tShould receive a status code of 401 for the response : %v", dbtests.Failed, w.Code)
		}
		t.Logf("\t%s\tTest:\tShould receive a status code of 401 for the response", dbtests.Success)
	}
}

func (at *PaymentTests) update400(t *testing.T) {
	const PaymentID = "ac22dc44-1778-4708-8d0e-f001c2ee0fbf"

	ua := payment.UpdatePayment{
		Gas: dbtests.FloatPointer(-1000), // invalid value
	}

	body, err := json.Marshal(ua)
	if err != nil {
		t.Fatal(err)
	}

	r := httptest.NewRequest(http.MethodPut, fmt.Sprintf("/v1/payment/%s", PaymentID), bytes.NewBuffer(body))
	w := httptest.NewRecorder()

	r.Header.Set("Authorization", "Bearer "+at.userWithData)
	at.app.ServeHTTP(w, r)

	t.Log("Given the need to update payment")
	{
		if w.Code != http.StatusBadRequest {
			t.Fatalf("\t%s\tTest:\tShould receive a status code of 400 for the response : %v", dbtests.Failed, w.Code)
		}
		t.Logf("\t%s\tTest:\tShould receive a status code of 400 for the response", dbtests.Success)
	}
}

func (at *PaymentTests) update404(t *testing.T) {
	randomUUID := uuid.New()

	ua := payment.UpdatePayment{
		Gas:        dbtests.FloatPointer(1000),
		GasTransit: dbtests.FloatPointer(2000),
		Light:      dbtests.FloatPointer(3000),
	}

	body, err := json.Marshal(ua)
	if err != nil {
		t.Fatal(err)
	}

	r := httptest.NewRequest(http.MethodPut, fmt.Sprintf("/v1/payment/%s", randomUUID), bytes.NewBuffer(body))
	w := httptest.NewRecorder()

	r.Header.Set("Authorization", "Bearer "+at.userWithData)
	at.app.ServeHTTP(w, r)

	t.Log("Given the need to update payment")
	{
		if w.Code != http.StatusNotFound {
			t.Fatalf("\t%s\tTest:\tShould receive a status code of 404 for the response : %v", dbtests.Failed, w.Code)
		}
		t.Logf("\t%s\tTest:\tShould receive a status code of 404 for the response", dbtests.Success)
	}
}

func (at *PaymentTests) update400InvalidID(t *testing.T) {
	const PaymentID = "invalidID"

	ua := payment.UpdatePayment{}

	body, err := json.Marshal(ua)
	if err != nil {
		t.Fatal(err)
	}

	r := httptest.NewRequest(http.MethodPut, fmt.Sprintf("/v1/payment/%s", PaymentID), strings.NewReader(string(body)))
	w := httptest.NewRecorder()

	r.Header.Set("Authorization", "Bearer "+at.userWithData)
	at.app.ServeHTTP(w, r)

	t.Log("Given the need to update address")
	{
		if w.Code != http.StatusBadRequest {
			t.Fatalf("\t%s\tTest:\tShould receive a status code of 400 for the response : %v", dbtests.Failed, w.Code)
		}
		t.Logf("\t%s\tTest:\tShould receive a status code of 400 for the response", dbtests.Success)
	}
}
