package tests

import (
	"bytes"
	"encoding/json"
	"fmt"
	"net/http"
	"net/http/httptest"
	"os"
	"strings"
	"testing"

	"github.com/google/uuid"
	"gitlab.com/NotDzhedai/family-budget/app/services/family-budget/handlers"
	"gitlab.com/NotDzhedai/family-budget/business/core/address"
	"gitlab.com/NotDzhedai/family-budget/business/data/dbtests"
)

// AddressTests holds methods for each address subtest.
type AddressTests struct {
	app http.Handler
	// юзер у якого немає адрес та платежів в бд
	userWithoutData string
	// юзер у якого є парочка адрес та платежів в бд
	userWithData string
	// юзер у якого немає адрес та платежів в бд. Потрібен для тесту пустого слвйсу
	emptyUserToken string
}

// TestAddress is the entry point for testing address management functions.
func TestAddress(t *testing.T) {
	test := dbtests.NewIntegration(t, c, "inittestaddress", migrationPath)
	t.Cleanup(test.Teardown)

	shutdown := make(chan os.Signal, 1)
	tests := AddressTests{
		app: handlers.APIMux(handlers.APIMuxConfig{
			Shutdown: shutdown,
			Log:      test.Log,
			Auth:     test.Auth,
			DB:       test.DB,
		}),
		userWithoutData: test.Token("user@example.com", "gophers"),
		userWithData:    test.Token("admin@example.com", "gophers"),
		emptyUserToken:  test.Token("empty@example.com", "gophers"),
	}

	// create
	t.Run("createAddress201", tests.createAddress201)
	t.Run("createAddress401", tests.createAddress401)
	t.Run("createAddress403", tests.createAddress403)
	t.Run("createAddress400", tests.createAddress400)
	// QueryByID
	t.Run("queryByID200", tests.queryByID200)
	t.Run("queryByID404", tests.queryByID404)
	t.Run("queryByID400", tests.queryByID400)
	// Query
	t.Run("query200", tests.query200)
	t.Run("query400", tests.query400)
	t.Run("query200empty", tests.query200empty)
	// Update
	t.Run("update204", tests.update204)
	t.Run("update401", tests.update401)
	t.Run("update400", tests.update400)
	t.Run("update404", tests.update404)
	t.Run("update400InvalidID", tests.update400InvalidID)
	// Delete
	t.Run("delete204", tests.delete204)
	t.Run("delete400", tests.delete400)
	t.Run("delete404", tests.delete404)
	t.Run("delete401", tests.delete401)

}

// TESTS FOR CREATE
// createAddress201 перевіряє нормальне створення адреси
func (at *AddressTests) createAddress201(t *testing.T) {
	na := address.NewAddress{
		Address: "testaddr",
	}

	body, err := json.Marshal(na)
	if err != nil {
		t.Fatal(err)
	}

	r := httptest.NewRequest(http.MethodPost, "/v1/address", bytes.NewBuffer(body))
	w := httptest.NewRecorder()

	r.Header.Set("Authorization", "Bearer "+at.userWithoutData)
	at.app.ServeHTTP(w, r)

	{
		if w.Code != http.StatusCreated {
			t.Fatalf("\t%s\tTest:\tShould receive a status code of 201 for the response : %v", dbtests.Failed, w.Code)
		}
		t.Logf("\t%s\tTest:\tShould receive a status code of 201 for the response", dbtests.Failed)
	}
}

// createAddress401 перевіряє чи авторизований юзер перед створенням адреси
func (at *AddressTests) createAddress401(t *testing.T) {
	na := address.NewAddress{
		Address: "testaddr",
	}

	body, err := json.Marshal(na)
	if err != nil {
		t.Fatal(err)
	}

	r := httptest.NewRequest(http.MethodPost, "/v1/address", bytes.NewBuffer(body))
	w := httptest.NewRecorder()

	r.Header.Set("Authorization", "invalid token")
	at.app.ServeHTTP(w, r)

	{
		if w.Code != http.StatusUnauthorized {
			t.Fatalf("\t%s\tTest:\tShould receive a status code of 401 for the response : %v", dbtests.Failed, w.Code)
		}
		t.Logf("\t%s\tTest:\tShould receive a status code of 401 for the response", dbtests.Success)
	}
}

// createAddress403 перевіряє помилку дублікату. Якщо адреса вже є в БД
func (at *AddressTests) createAddress403(t *testing.T) {
	na := address.NewAddress{
		Address: "test1", // dalready exist in db
	}

	body, err := json.Marshal(na)
	if err != nil {
		t.Fatal(err)
	}

	r := httptest.NewRequest(http.MethodPost, "/v1/address", bytes.NewBuffer(body))
	w := httptest.NewRecorder()

	r.Header.Set("Authorization", "Bearer "+at.userWithData)
	at.app.ServeHTTP(w, r)

	t.Log("Given the need to create a address")
	t.Log(w.Code)
	{
		if w.Code != http.StatusForbidden {
			t.Fatalf("\t%s\tTest:\tShould receive a status code of 403 for the response : %v", dbtests.Failed, w.Code)
		}
		t.Logf("\t%s\tTest:\tShould receive a status code of 403 for the response", dbtests.Success)
	}
}

// createAddress400 перевіряє помилку валідації. Недостатня довжина нової адреси
func (at *AddressTests) createAddress400(t *testing.T) {
	na := address.NewAddress{
		Address: "te", // invalid len
	}

	body, err := json.Marshal(na)
	if err != nil {
		t.Fatal(err)
	}

	r := httptest.NewRequest(http.MethodPost, "/v1/address", strings.NewReader(string(body)))
	w := httptest.NewRecorder()

	r.Header.Set("Authorization", "Bearer "+at.userWithoutData)
	at.app.ServeHTTP(w, r)

	t.Log("Given the need to create a address")
	t.Log(w.Code)
	{
		if w.Code != http.StatusBadRequest {
			t.Fatalf("\t%s\tTest:\tShould receive a status code of 400 for the response : %v", dbtests.Failed, w.Code)
		}
		t.Logf("\t%s\tTest:\tShould receive a status code of 400 for the response", dbtests.Success)
	}
}

// TESTS FOR QUERYBYID

// queryByID200 перевіряє отримання адреси за ID
func (at *AddressTests) queryByID200(t *testing.T) {
	const AddressID = "76b983d2-8c49-469c-864d-2c950d33d2d2"

	r := httptest.NewRequest(http.MethodGet, fmt.Sprintf("/v1/address/%s", AddressID), nil)
	w := httptest.NewRecorder()

	// userWithData only!
	r.Header.Set("Authorization", "Bearer "+at.userWithData)
	at.app.ServeHTTP(w, r)

	t.Log("Given the need to get address by id")
	{
		if w.Code != http.StatusOK {
			t.Fatalf("\t%s\tTest:\tShould receive a status code of 200 for the response : %v", dbtests.Failed, w.Code)
		}
		t.Logf("\t%s\tTest:\tShould receive a status code of 200 for the response", dbtests.Success)

		var got address.Address
		if err := json.NewDecoder(w.Body).Decode(&got); err != nil {
			t.Fatalf("\t%s\tTest:\tShould be able to unmarshal the response to an address type : %v", dbtests.Failed, err)
		}
		t.Logf("\t%s\tTest:\tShould be able to unmarshal the response to an address type.", dbtests.Success)

		if got.Address != "test1" {
			t.Logf("\tTest:\tGot: %s", got.Address)
			t.Logf("\tTest:\tExp: %s", "test1")
			t.Errorf("\t%s\tTest:\tShould be the same address", dbtests.Failed)
		}
		t.Logf("\t%s\tTest:\tShould be the same address", dbtests.Success)
	}
}

// queryByID404 перевіряє отримання 404 помилки. Шукаю адресу іншого юзера
func (at *AddressTests) queryByID404(t *testing.T) {
	const AddressID = "76b983d2-8c49-469c-864d-2c950d33d2d2"

	r := httptest.NewRequest(http.MethodGet, fmt.Sprintf("/v1/address/%s", AddressID), nil)
	w := httptest.NewRecorder()

	// Address belongs to userWithData. With userWithoutData -> 404
	r.Header.Set("Authorization", "Bearer "+at.userWithoutData)
	at.app.ServeHTTP(w, r)

	t.Log("Given the need to get address by id")
	{
		if w.Code != http.StatusNotFound {
			t.Fatalf("\t%s\tTest:\tShould receive a status code of 404 for the response : %v", dbtests.Failed, w.Code)
		}
		t.Logf("\t%s\tTest:\tShould receive a status code of 404 for the response", dbtests.Success)
	}
}

// queryByID400 перевіряє отримання 400 помилки передаючи невалідний ID
func (at *AddressTests) queryByID400(t *testing.T) {
	const InvalidAddressID = "76b983d2-8c49-469c-864d-2c950d33d2" // invalid

	r := httptest.NewRequest(http.MethodGet, fmt.Sprintf("/v1/address/%s", InvalidAddressID), nil)
	w := httptest.NewRecorder()

	r.Header.Set("Authorization", "Bearer "+at.userWithData)
	at.app.ServeHTTP(w, r)

	t.Log("Given the need to get address by id")
	{
		if w.Code != http.StatusBadRequest {
			t.Fatalf("\t%s\tTest:\tShould receive a status code of 400 for the response : %v", dbtests.Failed, w.Code)
		}
		t.Logf("\t%s\tTest:\tShould receive a status code of 400 for the response", dbtests.Success)
	}
}

// TESTS FOR QUERY

// query200 перевіряє отримання всіх адрес для юзера
func (at *AddressTests) query200(t *testing.T) {
	r := httptest.NewRequest(http.MethodGet, fmt.Sprintf("/v1/addresses/%d/%d", 1, 2), nil)
	w := httptest.NewRecorder()

	r.Header.Set("Authorization", "Bearer "+at.userWithData)
	at.app.ServeHTTP(w, r)

	t.Log("Given the need to query addresses")
	{
		if w.Code != http.StatusOK {
			t.Fatalf("\t%s\tTest:\tShould receive a status code of 200 for the response : %v", dbtests.Failed, w.Code)
		}
		t.Logf("\t%s\tTest:\tShould receive a status code of 200 for the response", dbtests.Success)

		var got []address.Address
		if err := json.NewDecoder(w.Body).Decode(&got); err != nil {
			t.Fatalf("\t%s\tTest:\tShould be able to unmarshal the response to an error type : %v", dbtests.Failed, err)
		}
		t.Logf("\t%s\tTest:\tShould be able to unmarshal the response to an error type", dbtests.Success)

		if len(got) != 2 {
			t.Fatalf("\t%s\tTest:\tShould get the len of 2. Got:%d", dbtests.Failed, len(got))
		}
		t.Logf("\t%s\tTest:\tShould get the len of 2. Got:%d", dbtests.Success, len(got))

		if got[0].Address != "test1" {
			t.Logf("\tTest:\tGot: %s", got[0].Address)
			t.Logf("\tTest:\tExp: %s", "test1")
			t.Errorf("\t%s\tTest:\tShould be the same address", dbtests.Failed)
		}
		t.Logf("\t%s\tTest:\tShould be the same address", dbtests.Success)

		if got[1].Address != "test2" {
			t.Logf("\tTest:\tGot: %s", got[1].Address)
			t.Logf("\tTest:\tExp: %s", "test2")
			t.Errorf("\t%s\tTest:\tShould be the same address", dbtests.Failed)
		}
		t.Logf("\t%s\tTest:\tShould be the same address", dbtests.Success)
	}
}

// query200empty перевіряє коли у юзера немає адрес. Повинен повернутись пустий слайс
func (at *AddressTests) query200empty(t *testing.T) {
	r := httptest.NewRequest(http.MethodGet, fmt.Sprintf("/v1/addresses/%d/%d", 1, 2), nil)
	w := httptest.NewRecorder()

	r.Header.Set("Authorization", "Bearer "+at.emptyUserToken)
	at.app.ServeHTTP(w, r)

	t.Log("Given the need to query addresses")
	{
		if w.Code != http.StatusOK {
			t.Fatalf("\t%s\tTest:\tShould receive a status code of 200 for the response : %v", dbtests.Failed, w.Code)
		}
		t.Logf("\t%s\tTest:\tShould receive a status code of 200 for the response", dbtests.Success)

		var got []address.Address
		if err := json.NewDecoder(w.Body).Decode(&got); err != nil {
			t.Fatalf("\t%s\tTest:\tShould be able to unmarshal the response to an error type : %v", dbtests.Failed, err)
		}
		t.Logf("\t%s\tTest:\tShould be able to unmarshal the response to an error type", dbtests.Success)

		if len(got) != 0 {
			t.Fatalf("\t%s\tTest:\tShould get the len of 0. Got:%d", dbtests.Failed, len(got))
		}
		t.Logf("\t%s\tTest:\tShould get the len of 0. Got:%d", dbtests.Success, len(got))
	}
}

// query400 перевіряє 400 помилку. Передача параметрів неправильного типу
func (at *AddressTests) query400(t *testing.T) {
	r := httptest.NewRequest(http.MethodGet, fmt.Sprintf("/v1/addresses/%f/%d", 1.3, 2), nil) // invalid param type
	w := httptest.NewRecorder()

	r.Header.Set("Authorization", "Bearer "+at.userWithData)
	at.app.ServeHTTP(w, r)

	t.Log("Given the need to query addresses")
	{
		if w.Code != http.StatusBadRequest {
			t.Fatalf("\t%s\tTest:\tShould receive a status code of 400 for the response : %v", dbtests.Failed, w.Code)
		}
		t.Logf("\t%s\tTest:\tShould receive a status code of 400 for the response", dbtests.Success)
	}
}

// TESTS FOR DELETE

// delete204 перевіряє видалення адреси
func (at *AddressTests) delete204(t *testing.T) {
	const AddressID = "76b983d2-8c49-469c-864d-2c950d33d2d2"

	r := httptest.NewRequest(http.MethodDelete, fmt.Sprintf("/v1/address/%s", AddressID), nil)
	w := httptest.NewRecorder()

	r.Header.Set("Authorization", "Bearer "+at.userWithData)
	at.app.ServeHTTP(w, r)

	t.Log("Given the need to delete address")
	{
		if w.Code != http.StatusNoContent {
			t.Fatalf("\t%s\tTest:\tShould receive a status code of 204 for the response : %v", dbtests.Failed, w.Code)
		}
		t.Logf("\t%s\tTest:\tShould receive a status code of 204 for the response", dbtests.Success)
	}

	// check id address deleted
	t.Log("Check if deleted")
	r = httptest.NewRequest(http.MethodGet, fmt.Sprintf("/v1/address/%s", AddressID), nil)
	w = httptest.NewRecorder()

	r.Header.Set("Authorization", "Bearer "+at.userWithData)
	at.app.ServeHTTP(w, r)

	if w.Code == http.StatusNotFound {
		t.Logf("\t%s\tTest:\tShould receive a status code of 404 for the response", dbtests.Success)
	}
}

// delete204 перевіряє 400 помилку. Невалідний ID
func (at *AddressTests) delete400(t *testing.T) {
	const InvalidAddressID = "invalidID"

	r := httptest.NewRequest(http.MethodDelete, fmt.Sprintf("/v1/address/%s", InvalidAddressID), nil)
	w := httptest.NewRecorder()

	r.Header.Set("Authorization", "Bearer "+at.userWithData)
	at.app.ServeHTTP(w, r)

	t.Log("Given the need to delete address")
	{
		if w.Code != http.StatusBadRequest {
			t.Fatalf("\t%s\tTest:\tShould receive a status code of 400 for the response : %v", dbtests.Failed, w.Code)
		}
		t.Logf("\t%s\tTest:\tShould receive a status code of 400 for the response", dbtests.Success)
	}
}

// delete404 перевіряє 404 помилку. Рандомний ID
func (at *AddressTests) delete404(t *testing.T) {
	randomUUID := uuid.New()

	r := httptest.NewRequest(http.MethodDelete, fmt.Sprintf("/v1/address/%s", randomUUID), nil)
	w := httptest.NewRecorder()

	r.Header.Set("Authorization", "Bearer "+at.userWithData)
	at.app.ServeHTTP(w, r)

	t.Log("Given the need to delete address")
	{
		if w.Code != http.StatusNotFound {
			t.Fatalf("\t%s\tTest:\tShould receive a status code of 404 for the response : %v", dbtests.Failed, w.Code)
		}
		t.Logf("\t%s\tTest:\tShould receive a status code of 404 for the response", dbtests.Success)
	}
}

// delete401 перевіряє 401 помилку. Неавторизований юзер
func (at *AddressTests) delete401(t *testing.T) {
	const AddressID = "76b983d2-8c49-469c-864d-2c950d33d2d2"

	r := httptest.NewRequest(http.MethodDelete, fmt.Sprintf("/v1/address/%s", AddressID), nil)
	w := httptest.NewRecorder()

	r.Header.Set("Authorization", "invalid token")
	at.app.ServeHTTP(w, r)

	t.Log("Given the need to delete address")
	{
		if w.Code != http.StatusUnauthorized {
			t.Fatalf("\t%s\tTest:\tShould receive a status code of 401 for the response : %v", dbtests.Failed, w.Code)
		}
		t.Logf("\t%s\tTest:\tShould receive a status code of 401 for the response", dbtests.Success)
	}
}

// TESTS FOR UPDATE

// update204 перевіряє оновлення адреси
func (at *AddressTests) update204(t *testing.T) {
	const AddressToUpdate = "76b983d2-8c49-112a-864d-2c950d33d23e"

	const UpdateAddr = "updated addr"
	ua := address.UpdateAddress{
		Address: UpdateAddr,
	}

	body, err := json.Marshal(ua)
	if err != nil {
		t.Fatal(err)
	}

	r := httptest.NewRequest(http.MethodPut, fmt.Sprintf("/v1/address/%s", AddressToUpdate), bytes.NewBuffer(body))
	w := httptest.NewRecorder()

	r.Header.Set("Authorization", "Bearer "+at.userWithData)
	at.app.ServeHTTP(w, r)

	t.Log("Given the need to update address")
	{
		if w.Code != http.StatusNoContent {
			t.Fatalf("\t%s\tTest:\tShould receive a status code of 204 for the response : %v", dbtests.Failed, w.Code)
		}
		t.Logf("\t%s\tTest:\tShould receive a status code of 204 for the response", dbtests.Success)
	}

	t.Log("Check if address updated")
	{
		r = httptest.NewRequest(http.MethodGet, fmt.Sprintf("/v1/address/%s", AddressToUpdate), nil)
		w = httptest.NewRecorder()

		r.Header.Set("Authorization", "Bearer "+at.userWithData)
		at.app.ServeHTTP(w, r)

		var got address.Address
		if err := json.NewDecoder(w.Body).Decode(&got); err != nil {
			t.Fatalf("\t%s\tTest:\tShould be able to unmarshal the response to an address type : %v", dbtests.Failed, err)
		}
		t.Logf("\t%s\tTest:\tShould be able to unmarshal the response to an address type.", dbtests.Success)

		if got.Address != UpdateAddr {
			t.Logf("\tTest:\tGot: %s", got.Address)
			t.Logf("\tTest:\tExp: %s", UpdateAddr)
			t.Errorf("\t%s\tTest:\tShould be the same address", dbtests.Failed)
		}
		t.Logf("\t%s\tTest:\tShould be the same address", dbtests.Success)
	}
}

// update401 перевіряє помилку 401. Неавторизований юзер
func (at *AddressTests) update401(t *testing.T) {
	const AddressToUpdate = "76b983d2-8c49-112a-864d-2c950d33d23e"

	ua := address.UpdateAddress{
		Address: "updated addr",
	}

	body, err := json.Marshal(ua)
	if err != nil {
		t.Fatal(err)
	}

	r := httptest.NewRequest(http.MethodPut, fmt.Sprintf("/v1/address/%s", AddressToUpdate), bytes.NewBuffer(body))
	w := httptest.NewRecorder()

	r.Header.Set("Authorization", "invalid token")
	at.app.ServeHTTP(w, r)

	t.Log("Given the need to update address")
	{
		if w.Code != http.StatusUnauthorized {
			t.Fatalf("\t%s\tTest:\tShould receive a status code of 401 for the response : %v", dbtests.Failed, w.Code)
		}
		t.Logf("\t%s\tTest:\tShould receive a status code of 401 for the response", dbtests.Success)
	}
}

// update401 перевіряє помилку 400. Недостатня довжина для нової адреси
func (at *AddressTests) update400(t *testing.T) {
	const AddressToUpdate = "76b983d2-8c49-112a-864d-2c950d33d23e"

	ua := address.UpdateAddress{
		Address: "up",
	}

	body, err := json.Marshal(ua)
	if err != nil {
		t.Fatal(err)
	}

	r := httptest.NewRequest(http.MethodPut, fmt.Sprintf("/v1/address/%s", AddressToUpdate), bytes.NewBuffer(body))
	w := httptest.NewRecorder()

	r.Header.Set("Authorization", "Bearer "+at.userWithData)
	at.app.ServeHTTP(w, r)

	t.Log("Given the need to update address")
	{
		if w.Code != http.StatusBadRequest {
			t.Fatalf("\t%s\tTest:\tShould receive a status code of 400 for the response : %v", dbtests.Failed, w.Code)
		}
		t.Logf("\t%s\tTest:\tShould receive a status code of 400 for the response", dbtests.Success)
	}
}

// update401 перевіряє помилку 404. Рандомний ID
func (at *AddressTests) update404(t *testing.T) {
	randomUUID := uuid.New()

	ua := address.UpdateAddress{
		Address: "updated addr",
	}

	body, err := json.Marshal(ua)
	if err != nil {
		t.Fatal(err)
	}

	r := httptest.NewRequest(http.MethodPut, fmt.Sprintf("/v1/address/%s", randomUUID), bytes.NewBuffer(body))
	w := httptest.NewRecorder()

	r.Header.Set("Authorization", "Bearer "+at.userWithData)
	at.app.ServeHTTP(w, r)

	t.Log("Given the need to update address")
	{
		if w.Code != http.StatusNotFound {
			t.Fatalf("\t%s\tTest:\tShould receive a status code of 404 for the response : %v", dbtests.Failed, w.Code)
		}
		t.Logf("\t%s\tTest:\tShould receive a status code of 404 for the response", dbtests.Success)
	}
}

// update401 перевіряє помилку 404. Невалідний ID
func (at *AddressTests) update400InvalidID(t *testing.T) {
	const InvalidAddress = "76b983d2-8c49-469c-864d-2c950d"

	ua := address.UpdateAddress{
		Address: "updated addr",
	}

	body, err := json.Marshal(ua)
	if err != nil {
		t.Fatal(err)
	}

	r := httptest.NewRequest(http.MethodPut, fmt.Sprintf("/v1/address/%s", InvalidAddress), strings.NewReader(string(body)))
	w := httptest.NewRecorder()

	r.Header.Set("Authorization", "Bearer "+at.userWithData)
	at.app.ServeHTTP(w, r)

	t.Log("Given the need to update address")
	{
		if w.Code != http.StatusBadRequest {
			t.Fatalf("\t%s\tTest:\tShould receive a status code of 400 for the response : %v", dbtests.Failed, w.Code)
		}
		t.Logf("\t%s\tTest:\tShould receive a status code of 400 for the response", dbtests.Success)
	}
}
