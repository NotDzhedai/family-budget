package tests

import (
	"fmt"
	"testing"

	"gitlab.com/NotDzhedai/family-budget/business/data/dbtests"
	"gitlab.com/NotDzhedai/family-budget/foundation/docker"
)

const migrationPath = "file://../../../../business/data/dbschema/sql/migrations"

var c *docker.Container

func TestMain(m *testing.M) {
	var err error
	c, err = dbtests.StartDB()
	if err != nil {
		fmt.Println(err)
		return
	}
	defer dbtests.StopDB(c)

	m.Run()
}
