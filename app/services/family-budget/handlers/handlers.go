// Package handlers contains the full set of handler functions and routes
// supported by the web api.
package handlers

import (
	"context"
	"expvar"
	"net/http"
	"net/http/pprof"
	"os"

	"github.com/jmoiron/sqlx"
	"gitlab.com/NotDzhedai/family-budget/app/services/family-budget/handlers/debug/checkgrp"
	v1addressgrp "gitlab.com/NotDzhedai/family-budget/app/services/family-budget/handlers/v1/addressgrp"
	v1paymentgrp "gitlab.com/NotDzhedai/family-budget/app/services/family-budget/handlers/v1/paymentgrp"
	v1testgrp "gitlab.com/NotDzhedai/family-budget/app/services/family-budget/handlers/v1/testgrp"
	v1usergrp "gitlab.com/NotDzhedai/family-budget/app/services/family-budget/handlers/v1/usergrp"
	addressCore "gitlab.com/NotDzhedai/family-budget/business/core/address"
	paymentCore "gitlab.com/NotDzhedai/family-budget/business/core/payment"
	userCore "gitlab.com/NotDzhedai/family-budget/business/core/user"
	"gitlab.com/NotDzhedai/family-budget/business/sys/auth"
	"gitlab.com/NotDzhedai/family-budget/business/web/v1/mid"
	"gitlab.com/NotDzhedai/family-budget/foundation/web"
	"go.uber.org/zap"
)

// Options represent optional parameters.
type Options struct {
	corsOrigin string
}

// WithCORS provides configuration options for CORS.
func WithCORS(origin string) func(opts *Options) {
	return func(opts *Options) {
		opts.corsOrigin = origin
	}
}

// DebugStandartLibraryMux registers all the debug routes from the standard library
// into a new mux bypassing the use of the DefaultServerMux. Using the
// DefaultServerMux would be a security risk since a dependency could inject a
// handler into our service without us knowing it.
func DebugStandartLibraryMux() *http.ServeMux {
	mux := http.NewServeMux()

	// Register all the standard library debug endpoints.
	mux.HandleFunc("/debug/pprof/", pprof.Index)
	mux.HandleFunc("/debug/pprof/cmdline", pprof.Cmdline)
	mux.HandleFunc("/debug/pprof/profile", pprof.Profile)
	mux.HandleFunc("/debug/pprof/symbol", pprof.Symbol)
	mux.HandleFunc("/debug/pprof/trace", pprof.Trace)
	mux.Handle("/debug/vars", expvar.Handler())

	return mux
}

// DebugMux registers all the debug standard library routes and then custom
// debug application routes for the service. This bypassing the use of the
// DefaultServerMux. Using the DefaultServerMux would be a security risk since
// a dependency could inject a handler into our service without us knowing it.
func DebugMux(build string, log *zap.SugaredLogger, db *sqlx.DB) http.Handler {
	mux := DebugStandartLibraryMux()

	checkGrp := checkgrp.Handlers{
		Build: build,
		Log:   log,
		DB:    db,
	}

	mux.HandleFunc("/debug/readiness", checkGrp.Readiness)
	mux.HandleFunc("/debug/liveness", checkGrp.Liveness)

	return mux
}

// APIMuxConfig contains all the mandatory systems required by handlers.
type APIMuxConfig struct {
	Shutdown chan os.Signal
	Log      *zap.SugaredLogger
	Auth     *auth.Auth
	DB       *sqlx.DB
}

// APIMux constructs an http.Handler with all application routes defined.
func APIMux(cfg APIMuxConfig, options ...func(opts *Options)) *web.App {
	var opts Options
	for _, option := range options {
		option(&opts)
	}

	// Construct the web.App which holds all routes as well as common Middleware.
	var app *web.App

	// Do we need CORS?
	if opts.corsOrigin != "" {
		app = web.NewApp(
			cfg.Shutdown,
			mid.Logger(cfg.Log),
			mid.Errors(cfg.Log),
			mid.Metrics(),
			mid.Cors(opts.corsOrigin),
			mid.Panics(),
		)

		// Accept CORS 'OPTIONS' preflight requests if config has been provided.
		// Don't forget to apply the CORS middleware to the routes that need it.
		// Example Config: `conf:"default:https://MY_DOMAIN.COM"`
		h := func(ctx context.Context, w http.ResponseWriter, r *http.Request) error {
			return nil
		}
		app.Handle(http.MethodOptions, "", "/*", h, mid.Cors(opts.corsOrigin))
	}

	if app == nil {
		app = web.NewApp(
			cfg.Shutdown,
			mid.Logger(cfg.Log),
			mid.Errors(cfg.Log),
			mid.Metrics(),
			mid.Panics(),
		)
	}

	v1(app, cfg)

	return app
}

func v1(app *web.App, cfg APIMuxConfig) {
	const version = "v1"

	testGrp := v1testgrp.Handlers{
		Log: cfg.Log,
	}

	app.Handle(http.MethodGet, version, "/test", testGrp.Test)
	app.Handle(http.MethodGet, version, "/testauth", testGrp.Test, mid.Authenticate(cfg.Auth))

	ugh := v1usergrp.Handlers{
		Core: userCore.NewCore(cfg.Log, cfg.DB),
		Auth: cfg.Auth,
	}

	app.Handle(http.MethodGet, version, "/users/token", ugh.Token)
	app.Handle(http.MethodGet, version, "/users/:page/:rows", ugh.Query, mid.Authenticate(cfg.Auth)) // Do not need?
	app.Handle(http.MethodGet, version, "/users", ugh.QueryByID, mid.Authenticate(cfg.Auth))
	app.Handle(http.MethodPost, version, "/users", ugh.Create)
	app.Handle(http.MethodPut, version, "/users", ugh.Update, mid.Authenticate(cfg.Auth))
	app.Handle(http.MethodDelete, version, "/users", ugh.Delete, mid.Authenticate(cfg.Auth))

	addrgh := v1addressgrp.Handlers{
		Core: addressCore.NewCore(cfg.Log, cfg.DB),
		Auth: cfg.Auth,
	}

	app.Handle(http.MethodPost, version, "/address", addrgh.Create, mid.Authenticate(cfg.Auth))
	app.Handle(http.MethodGet, version, "/address/:id", addrgh.QueryByID, mid.Authenticate(cfg.Auth))
	app.Handle(http.MethodGet, version, "/address", addrgh.QueryByAddress, mid.Authenticate(cfg.Auth))
	app.Handle(http.MethodDelete, version, "/address/:id", addrgh.Delete, mid.Authenticate(cfg.Auth))
	app.Handle(http.MethodPut, version, "/address/:id", addrgh.Update, mid.Authenticate(cfg.Auth))
	app.Handle(http.MethodGet, version, "/addresses/:page/:rows", addrgh.Query, mid.Authenticate(cfg.Auth)) // test

	pmtgh := v1paymentgrp.Handlers{
		Core: paymentCore.NewCore(cfg.Log, cfg.DB),
		Auth: cfg.Auth,
	}

	app.Handle(http.MethodPost, version, "/payment", pmtgh.Create, mid.Authenticate(cfg.Auth))
	app.Handle(http.MethodGet, version, "/payment/:id", pmtgh.QueryByID, mid.Authenticate(cfg.Auth))
	app.Handle(http.MethodDelete, version, "/payment/:id", pmtgh.Delete, mid.Authenticate(cfg.Auth))
	app.Handle(http.MethodGet, version, "/payments/:address_id/:page/:rows", pmtgh.QueryByAddress, mid.Authenticate(cfg.Auth))
	app.Handle(http.MethodPut, version, "/payment/:id", pmtgh.Update, mid.Authenticate(cfg.Auth))
}
