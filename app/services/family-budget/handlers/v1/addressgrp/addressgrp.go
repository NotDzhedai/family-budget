// Package addressgrp contains all the address handlers.
package addressgrp

import (
	"context"
	"errors"
	"fmt"
	"net/http"
	"strconv"

	"gitlab.com/NotDzhedai/family-budget/business/core/address"
	"gitlab.com/NotDzhedai/family-budget/business/sys/auth"
	v1web "gitlab.com/NotDzhedai/family-budget/business/web/v1"
	"gitlab.com/NotDzhedai/family-budget/foundation/web"
)

type Handlers struct {
	Core address.Core
	Auth *auth.Auth
}

func (h Handlers) Create(ctx context.Context, w http.ResponseWriter, r *http.Request) error {
	claims, err := auth.GetClaims(ctx)
	if err != nil {
		return v1web.NewRequestError(auth.ErrForbidden, http.StatusForbidden)
	}

	v, err := web.GetValues(ctx)
	if err != nil {
		return web.NewShutdownError("web value missing from context")
	}

	var na address.NewAddress
	if err := web.Decode(r, &na); err != nil {
		return fmt.Errorf("unable to decode payload: %w", err)
	}

	addr, err := h.Core.Create(ctx, claims.Subject, na, v.Now)
	if err != nil {
		switch {
		case errors.Is(err, address.ErrDublicateKey):
			return v1web.NewRequestError(err, http.StatusForbidden)
		default:
			return fmt.Errorf("user[%+v]: %w", &addr, err)
		}
	}

	return web.Respond(ctx, w, addr, http.StatusCreated)
}

func (h Handlers) QueryByID(ctx context.Context, w http.ResponseWriter, r *http.Request) error {
	claims, err := auth.GetClaims(ctx)
	if err != nil {
		return v1web.NewRequestError(auth.ErrForbidden, http.StatusForbidden)
	}

	addrID := web.Param(r, "id")

	addr, err := h.Core.QueryByID(ctx, addrID, claims.Subject)
	if err != nil {
		switch {
		case errors.Is(err, address.ErrInvalidID):
			return v1web.NewRequestError(err, http.StatusBadRequest)
		case errors.Is(err, address.ErrNotFound):
			return v1web.NewRequestError(err, http.StatusNotFound)
		default:
			return fmt.Errorf("ID[%s]: %w", addrID, err)
		}
	}

	return web.Respond(ctx, w, addr, http.StatusOK)
}

func (h Handlers) QueryByAddress(ctx context.Context, w http.ResponseWriter, r *http.Request) error {
	claims, err := auth.GetClaims(ctx)
	if err != nil {
		return v1web.NewRequestError(auth.ErrForbidden, http.StatusForbidden)
	}

	var addrToSearch struct {
		Address string `json:"address"`
	}
	if err := web.Decode(r, &addrToSearch); err != nil {
		return fmt.Errorf("unable to decode payload: %w", err)
	}

	addr, err := h.Core.QueryByAddress(ctx, addrToSearch.Address, claims.Subject)
	if err != nil {
		switch {
		case errors.Is(err, address.ErrInvalidID):
			return v1web.NewRequestError(err, http.StatusBadRequest)
		case errors.Is(err, address.ErrNotFound):
			return v1web.NewRequestError(err, http.StatusNotFound)
		default:
			return fmt.Errorf("address[%s]: %w", addrToSearch.Address, err)
		}
	}

	return web.Respond(ctx, w, addr, http.StatusOK)
}

func (h Handlers) Delete(ctx context.Context, w http.ResponseWriter, r *http.Request) error {
	claims, err := auth.GetClaims(ctx)
	if err != nil {
		return v1web.NewRequestError(auth.ErrForbidden, http.StatusForbidden)
	}

	addrID := web.Param(r, "id")

	if err := h.Core.Delete(ctx, addrID, claims.Subject); err != nil {
		switch {
		case errors.Is(err, address.ErrInvalidID):
			return v1web.NewRequestError(err, http.StatusBadRequest)
		case errors.Is(err, address.ErrNotFound):
			return v1web.NewRequestError(err, http.StatusNotFound)
		default:
			return fmt.Errorf("ID[%s]: %w", addrID, err)
		}
	}

	return web.Respond(ctx, w, nil, http.StatusNoContent)
}

func (h Handlers) Update(ctx context.Context, w http.ResponseWriter, r *http.Request) error {
	v, err := web.GetValues(ctx)
	if err != nil {
		return web.NewShutdownError("web value missing from context")
	}

	claims, err := auth.GetClaims(ctx)
	if err != nil {
		return v1web.NewRequestError(auth.ErrForbidden, http.StatusForbidden)
	}

	addrID := web.Param(r, "id")

	var addrToUpdate address.UpdateAddress
	if err := web.Decode(r, &addrToUpdate); err != nil {
		return fmt.Errorf("unable to decode payload: %w", err)
	}

	if err := h.Core.Update(ctx, addrID, claims.Subject, addrToUpdate, v.Now); err != nil {
		switch {
		case errors.Is(err, address.ErrInvalidID):
			return v1web.NewRequestError(err, http.StatusBadRequest)
		case errors.Is(err, address.ErrNotFound):
			return v1web.NewRequestError(err, http.StatusNotFound)
		default:
			return fmt.Errorf("ID[%s] NewAddress[%v]: %w", claims.Subject, addrToUpdate.Address, err)
		}
	}

	return web.Respond(ctx, w, nil, http.StatusNoContent)
}

func (h Handlers) Query(ctx context.Context, w http.ResponseWriter, r *http.Request) error {
	claims, err := auth.GetClaims(ctx)
	if err != nil {
		return v1web.NewRequestError(auth.ErrForbidden, http.StatusForbidden)
	}

	page := web.Param(r, "page")
	pageNumber, err := strconv.Atoi(page)
	if err != nil {
		return v1web.NewRequestError(fmt.Errorf("invalid page format [%s]", page), http.StatusBadRequest)
	}

	rows := web.Param(r, "rows")
	rowsPerPage, err := strconv.Atoi(rows)
	if err != nil {
		return v1web.NewRequestError(fmt.Errorf("invalid rows format [%s]", rows), http.StatusBadRequest)
	}

	users, err := h.Core.Query(ctx, claims.Subject, pageNumber, rowsPerPage)
	if err != nil {
		return fmt.Errorf("unable to query for users: %w", err)
	}

	return web.Respond(ctx, w, users, http.StatusOK)
}
