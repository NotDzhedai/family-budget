// Package testgrp contains all the test handlers.
package testgrp

import (
	"context"
	"errors"
	"math/rand"
	"net/http"

	v1web "gitlab.com/NotDzhedai/family-budget/business/web/v1"
	"gitlab.com/NotDzhedai/family-budget/foundation/web"
	"go.uber.org/zap"
)

// Handlers manages the set of check enpoints.
type Handlers struct {
	Log *zap.SugaredLogger
}

// Test handler is for development.
func (h Handlers) Test(ctx context.Context, w http.ResponseWriter, r *http.Request) error {
	if n := rand.Intn(100); n%2 == 0 {
		return v1web.NewRequestError(errors.New("trusted error"), http.StatusBadRequest)
		// panic("Testing panic")
	}

	status := struct {
		Status string
	}{
		Status: "Hello",
	}

	return web.Respond(ctx, w, status, http.StatusOK)
}
