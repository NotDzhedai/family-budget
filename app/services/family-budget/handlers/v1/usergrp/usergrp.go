// Package usergrp contains all the user handlers.
package usergrp

import (
	"context"
	"errors"
	"fmt"
	"net/http"
	"strconv"

	"gitlab.com/NotDzhedai/family-budget/business/core/user"
	"gitlab.com/NotDzhedai/family-budget/business/sys/auth"
	v1web "gitlab.com/NotDzhedai/family-budget/business/web/v1"
	"gitlab.com/NotDzhedai/family-budget/foundation/web"
)

type Handlers struct {
	Core user.Core
	Auth *auth.Auth
}

// Query  Don't use for now
func (h Handlers) Query(ctx context.Context, w http.ResponseWriter, r *http.Request) error {
	page := web.Param(r, "page")
	pageNumber, err := strconv.Atoi(page)
	if err != nil {
		return v1web.NewRequestError(fmt.Errorf("invalid page format [%s]", page), http.StatusBadRequest)
	}

	rows := web.Param(r, "rows")
	rowsPerPage, err := strconv.Atoi(rows)
	if err != nil {
		return v1web.NewRequestError(fmt.Errorf("invalid rows format [%s]", rows), http.StatusBadRequest)
	}

	users, err := h.Core.Query(ctx, pageNumber, rowsPerPage)
	if err != nil {
		return fmt.Errorf("unable to query for users: %w", err)
	}

	return web.Respond(ctx, w, users, http.StatusOK)
}

func (h Handlers) QueryByID(ctx context.Context, w http.ResponseWriter, r *http.Request) error {
	claims, err := auth.GetClaims(ctx)
	if err != nil {
		return v1web.NewRequestError(auth.ErrForbidden, http.StatusForbidden)
	}

	usr, err := h.Core.QueryByID(ctx, claims.Subject)
	if err != nil {
		switch {
		case errors.Is(err, user.ErrInvalidID):
			return v1web.NewRequestError(err, http.StatusBadRequest)
		case errors.Is(err, user.ErrNotFound):
			return v1web.NewRequestError(err, http.StatusNotFound)
		default:
			return fmt.Errorf("ID[%s]: %w", claims.Subject, err)
		}
	}

	return web.Respond(ctx, w, usr, http.StatusOK)
}

func (h Handlers) Create(ctx context.Context, w http.ResponseWriter, r *http.Request) error {
	v, err := web.GetValues(ctx)
	if err != nil {
		return web.NewShutdownError("web value missing from context")
	}

	var nu user.NewUser
	if err := web.Decode(r, &nu); err != nil {
		return fmt.Errorf("unable to decode payload: %w", err)
	}

	usr, err := h.Core.Create(ctx, nu, v.Now)
	if err != nil {
		switch {
		case errors.Is(err, user.ErrDublicateKey):
			return v1web.NewRequestError(err, http.StatusForbidden)
		default:
			return fmt.Errorf("user[%+v]: %w", &usr, err)
		}
	}

	return web.Respond(ctx, w, usr, http.StatusCreated)
}

func (h Handlers) Update(ctx context.Context, w http.ResponseWriter, r *http.Request) error {
	v, err := web.GetValues(ctx)
	if err != nil {
		return web.NewShutdownError("web value missing from context")
	}

	claims, err := auth.GetClaims(ctx)
	if err != nil {
		return v1web.NewRequestError(auth.ErrForbidden, http.StatusForbidden)
	}

	var upd user.UpdateUser
	if err := web.Decode(r, &upd); err != nil {
		return fmt.Errorf("unable to decode payload: %w", err)
	}

	if err := h.Core.Update(ctx, claims.Subject, upd, v.Now); err != nil {
		switch {
		case errors.Is(err, user.ErrInvalidID):
			return v1web.NewRequestError(err, http.StatusBadRequest)
		case errors.Is(err, user.ErrNotFound):
			return v1web.NewRequestError(err, http.StatusNotFound)
		default:
			return fmt.Errorf("ID[%s] User[%+v]: %w", claims.Subject, &upd, err)
		}
	}

	return web.Respond(ctx, w, nil, http.StatusNoContent)
}

func (h Handlers) Delete(ctx context.Context, w http.ResponseWriter, r *http.Request) error {
	claims, err := auth.GetClaims(ctx)
	if err != nil {
		return errors.New("claims missing from context")
	}

	if err := h.Core.Delete(ctx, claims.Subject); err != nil {
		switch {
		case errors.Is(err, user.ErrInvalidID):
			return v1web.NewRequestError(err, http.StatusBadRequest)
		case errors.Is(err, user.ErrNotFound):
			return v1web.NewRequestError(err, http.StatusNotFound)
		default:
			return fmt.Errorf("ID[%s]: %w", claims.Subject, err)
		}
	}

	return web.Respond(ctx, w, nil, http.StatusNoContent)
}

// Token provides an API token for the authenticated user.
func (h Handlers) Token(ctx context.Context, w http.ResponseWriter, r *http.Request) error {
	v, err := web.GetValues(ctx)
	if err != nil {
		return web.NewShutdownError("web value missing from context")
	}

	email, pass, ok := r.BasicAuth()
	if !ok {
		err := errors.New("must provide email and password in Basic auth")
		return v1web.NewRequestError(err, http.StatusUnauthorized)
	}

	claims, err := h.Core.Authenticate(ctx, v.Now, email, pass)
	if err != nil {
		switch {
		case errors.Is(err, user.ErrNotFound):
			return v1web.NewRequestError(err, http.StatusNotFound)
		case errors.Is(err, user.ErrAuthenticationFailure):
			return v1web.NewRequestError(err, http.StatusUnauthorized)
		default:
			return fmt.Errorf("authenticating: %w", err)
		}
	}

	var tkn struct {
		Token string `json:"token"`
	}
	tkn.Token, err = h.Auth.GenerateToken(claims)
	if err != nil {
		return fmt.Errorf("generating token: %w", err)
	}

	return web.Respond(ctx, w, tkn, http.StatusOK)
}
