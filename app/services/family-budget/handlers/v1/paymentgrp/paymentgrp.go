// Package paymentgrp contains all the payment handlers.
package paymentgrp

import (
	"context"
	"errors"
	"fmt"
	"net/http"
	"strconv"

	"gitlab.com/NotDzhedai/family-budget/business/core/payment"
	"gitlab.com/NotDzhedai/family-budget/business/sys/auth"
	v1web "gitlab.com/NotDzhedai/family-budget/business/web/v1"
	"gitlab.com/NotDzhedai/family-budget/foundation/web"
)

type Handlers struct {
	Core payment.Core
	Auth *auth.Auth
}

func (h Handlers) Create(ctx context.Context, w http.ResponseWriter, r *http.Request) error {
	claims, err := auth.GetClaims(ctx)
	if err != nil {
		return v1web.NewRequestError(auth.ErrForbidden, http.StatusForbidden)
	}

	v, err := web.GetValues(ctx)
	if err != nil {
		return web.NewShutdownError("web value missing from context")
	}

	var np payment.NewPayment
	if err := web.Decode(r, &np); err != nil {
		return fmt.Errorf("unable to decode payload: %w", err)
	}

	pmt, err := h.Core.Create(ctx, np, claims.Subject, v.Now)
	if err != nil {
		switch {
		case errors.Is(err, payment.ErrNotFoundForeignKey):
			return v1web.NewRequestError(err, http.StatusBadRequest)
		default:
			return fmt.Errorf("user[%+v]: %w", &pmt, err)
		}
	}

	return web.Respond(ctx, w, pmt, http.StatusCreated)
}

func (h Handlers) QueryByID(ctx context.Context, w http.ResponseWriter, r *http.Request) error {
	claims, err := auth.GetClaims(ctx)
	if err != nil {
		return v1web.NewRequestError(auth.ErrForbidden, http.StatusForbidden)
	}

	pmtID := web.Param(r, "id")

	addr, err := h.Core.QueryByID(ctx, pmtID, claims.Subject)
	if err != nil {
		switch {
		case errors.Is(err, payment.ErrInvalidID):
			return v1web.NewRequestError(err, http.StatusBadRequest)
		case errors.Is(err, payment.ErrNotFound):
			return v1web.NewRequestError(err, http.StatusNotFound)
		default:
			return fmt.Errorf("ID[%s]: %w", pmtID, err)
		}
	}

	if addr.UserID != claims.Subject {
		return v1web.NewRequestError(auth.ErrForbidden, http.StatusForbidden)
	}

	return web.Respond(ctx, w, addr, http.StatusOK)
}

func (h Handlers) Delete(ctx context.Context, w http.ResponseWriter, r *http.Request) error {
	claims, err := auth.GetClaims(ctx)
	if err != nil {
		return errors.New("claims missing from context")
	}

	pmtID := web.Param(r, "id")

	if err := h.Core.Delete(ctx, pmtID, claims.Subject); err != nil {
		switch {
		case errors.Is(err, payment.ErrInvalidID):
			return v1web.NewRequestError(err, http.StatusBadRequest)
		case errors.Is(err, payment.ErrNotFound):
			return v1web.NewRequestError(err, http.StatusNotFound)
		default:
			return fmt.Errorf("ID[%s]: %w", pmtID, err)
		}
	}

	return web.Respond(ctx, w, nil, http.StatusNoContent)
}

func (h Handlers) QueryByAddress(ctx context.Context, w http.ResponseWriter, r *http.Request) error {
	claims, err := auth.GetClaims(ctx)
	if err != nil {
		return errors.New("claims missing from context")
	}

	page := web.Param(r, "page")
	pageNumber, err := strconv.Atoi(page)
	if err != nil {
		return v1web.NewRequestError(fmt.Errorf("invalid page format [%s]", page), http.StatusBadRequest)
	}

	rows := web.Param(r, "rows")
	rowsPerPage, err := strconv.Atoi(rows)
	if err != nil {
		return v1web.NewRequestError(fmt.Errorf("invalid rows format [%s]", rows), http.StatusBadRequest)
	}

	addrID := web.Param(r, "address_id")

	payments, err := h.Core.Query(ctx, claims.Subject, addrID, pageNumber, rowsPerPage)
	if err != nil {
		switch {
		case errors.Is(err, payment.ErrInvalidID):
			return v1web.NewRequestError(err, http.StatusBadRequest)
		default:
			return fmt.Errorf("unable to query for payments: %w", err)
		}
	}

	pages, err := h.Core.GetPaymentsPagesLen(ctx, claims.Subject, addrID, rowsPerPage)
	if err != nil {
		switch {
		case errors.Is(err, payment.ErrInvalidID):
			return v1web.NewRequestError(err, http.StatusBadRequest)
		default:
			return fmt.Errorf("unable to query for payments: %w", err)
		}
	}

	res := struct {
		Payments []payment.Payment `json:"payments"`
		Pages    int               `json:"pages"`
	}{
		Payments: payments,
		Pages:    pages,
	}

	return web.Respond(ctx, w, res, http.StatusOK)
}

func (h Handlers) Update(ctx context.Context, w http.ResponseWriter, r *http.Request) error {
	v, err := web.GetValues(ctx)
	if err != nil {
		return web.NewShutdownError("web value missing from context")
	}

	claims, err := auth.GetClaims(ctx)
	if err != nil {
		return v1web.NewRequestError(auth.ErrForbidden, http.StatusForbidden)
	}

	var upp payment.UpdatePayment
	if err := web.Decode(r, &upp); err != nil {
		return fmt.Errorf("unable to decode payload: %w", err)
	}

	pmtID := web.Param(r, "id")

	if err := h.Core.Update(ctx, pmtID, claims.Subject, upp, v.Now); err != nil {
		switch {
		case errors.Is(err, payment.ErrInvalidID):
			return v1web.NewRequestError(err, http.StatusBadRequest)
		case errors.Is(err, payment.ErrNotFound):
			return v1web.NewRequestError(err, http.StatusNotFound)
		default:
			return fmt.Errorf("ID[%s] User[%+v]: %w", claims.Subject, &upp, err)
		}
	}

	return web.Respond(ctx, w, nil, http.StatusNoContent)
}
