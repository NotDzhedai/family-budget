import { HttpErrorResponse } from "@angular/common/http";
import { Component, OnDestroy, OnInit } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { Router } from "@angular/router";
import { Subject, Subscription, takeUntil } from "rxjs";
import { Err } from "../shared/data/error/Err";
import { AuthService } from "../shared/services/auth.service";

@Component({
  selector: "app-login-page",
  templateUrl: "./login-page.component.html",
  styleUrls: ["./login-page.component.css"],
})
export class LoginPageComponent implements OnInit, OnDestroy {
  notifier = new Subject<void>();

  loginForm: FormGroup = new FormGroup({});
  err: Err;

  constructor(
    private fb: FormBuilder,
    private auth: AuthService,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.loginForm = this.fb.group({
      email: this.fb.control(null, [Validators.required, Validators.email]),
      password: this.fb.control(null, [
        Validators.required,
        Validators.minLength(5),
      ]),
    });
  }

  ngOnDestroy(): void {
    this.notifier.next();
    this.notifier.complete();
  }

  onSubmit() {
    this.loginForm.disable();
    this.auth
      .login(
        this.loginForm.controls["email"].value,
        this.loginForm.controls["password"].value
      )
      .pipe(takeUntil(this.notifier))
      .subscribe({
        next: () => {
          this.router.navigate(["/overview"]);
        },
        error: (error: HttpErrorResponse) => {
          switch (error.status) {
            case 400:
              this.err = new Err(error.error.error, error.error.fields);
              break;
            default:
              this.err = new Err(error.error.error, {});
              break;
          }
          this.loginForm.reset();
          this.loginForm.enable();
        },
      });
  }

  get email() {
    return this.loginForm.get("email");
  }
  get password() {
    return this.loginForm.get("password");
  }
}
