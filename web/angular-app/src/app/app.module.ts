import { NgModule } from "@angular/core";
import { BrowserModule } from "@angular/platform-browser";

import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";
import { AuthLayoutComponent } from "./shared/layouts/auth-layout/auth-layout.component";
import { SiteLayoutComponent } from "./shared/layouts/site-layout/site-layout.component";
import { LoginPageComponent } from "./login-page/login-page.component";
import { RegisterPageComponent } from "./register-page/register-page.component";
import { OverviewPageComponent } from "./overview-page/overview-page.component";
import { HttpClientModule, HTTP_INTERCEPTORS } from "@angular/common/http";
import { TokenInterceptor } from "./shared/classes/token.interceptor";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { NewPageComponent } from "./new-page/new-page.component";
import { AddressComponent } from "./new-page/address/address.component";
import { PaymentComponent } from "./new-page/payment/payment.component";
import { LoaderComponent } from "./shared/components/loader/loader.component";
import { PaymentsChartComponent } from "./overview-page/payments-chart/payments-chart.component";
import { NgxChartsModule } from "@swimlane/ngx-charts";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { PaymentPageComponent } from "./payment-page/payment-page.component";
import { ErrorComponent } from "./shared/components/error/error.component";
import { AddressesListComponent } from "./new-page/addresses-list/addresses-list.component";

@NgModule({
  declarations: [
    AppComponent,
    AuthLayoutComponent,
    SiteLayoutComponent,
    LoginPageComponent,
    RegisterPageComponent,
    OverviewPageComponent,
    NewPageComponent,
    AddressComponent,
    PaymentComponent,
    LoaderComponent,
    PaymentsChartComponent,
    PaymentPageComponent,
    ErrorComponent,
    AddressesListComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    ReactiveFormsModule,
    FormsModule,
    NgxChartsModule,
    BrowserAnimationsModule,
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      multi: true,
      useClass: TokenInterceptor,
    },
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
