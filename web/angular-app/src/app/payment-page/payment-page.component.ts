import { AfterViewInit, Component, OnDestroy, OnInit } from "@angular/core";
import { ActivatedRoute } from "@angular/router";
import { Subject, takeUntil } from "rxjs";
import { ChartItem, PaymentChartData } from "../shared/data/chart/chart";
import { Payment } from "../shared/data/payment/interface";
import { PaymentService } from "../shared/services/payment.service";

@Component({
  selector: "app-payment-page",
  templateUrl: "./payment-page.component.html",
  styleUrls: ["./payment-page.component.css"],
})
export class PaymentPageComponent implements OnInit, OnDestroy {
  notifier = new Subject<void>();

  pmtID: string;
  payment: Payment;

  chart: PaymentChartData;
  chartData: ChartItem[];

  constructor(
    private route: ActivatedRoute,
    public paymentService: PaymentService
  ) {}

  ngOnDestroy(): void {
    this.notifier.next();
    this.notifier.complete();
  }

  ngOnInit(): void {
    this.route.params.subscribe((params) => {
      this.pmtID = params["id"];
    });

    this.paymentService.getSinglePayment(this.pmtID);

    this.paymentService.payment$
      .pipe(takeUntil(this.notifier))
      .subscribe((data) => {
        if (data) {
          this.chart = new PaymentChartData(data);
          this.chartData = this.chart.getDataForChart();
        }
      });
  }
}
