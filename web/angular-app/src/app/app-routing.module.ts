import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { NewPageComponent } from "./new-page/new-page.component";
import { LoginPageComponent } from "./login-page/login-page.component";
import { OverviewPageComponent } from "./overview-page/overview-page.component";
import { RegisterPageComponent } from "./register-page/register-page.component";
import { AuthGuard } from "./shared/classes/auth.guard";
import { AuthLayoutComponent } from "./shared/layouts/auth-layout/auth-layout.component";
import { SiteLayoutComponent } from "./shared/layouts/site-layout/site-layout.component";
import { PaymentPageComponent } from "./payment-page/payment-page.component";

const routes: Routes = [
  {
    path: "",
    component: AuthLayoutComponent,
    children: [
      { path: "", redirectTo: "/login", pathMatch: "full" },
      { path: "login", component: LoginPageComponent }, //login
      { path: "register", component: RegisterPageComponent }, //register
    ],
  },
  {
    path: "",
    canActivate: [AuthGuard],
    component: SiteLayoutComponent,
    children: [
      { path: "overview", component: OverviewPageComponent },
      { path: "new", component: NewPageComponent },
      { path: "payment/:id", component: PaymentPageComponent },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
