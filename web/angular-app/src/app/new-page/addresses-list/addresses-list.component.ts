import { Component, EventEmitter, Input, OnInit, Output } from "@angular/core";
import { Address } from "src/app/shared/data/address/interface";

@Component({
  selector: "app-addresses-list",
  templateUrl: "./addresses-list.component.html",
  styleUrls: ["./addresses-list.component.css"],
})
export class AddressesListComponent implements OnInit {
  @Input() loading = false;
  @Input() addresses: Address[];

  @Output() delete: EventEmitter<string> = new EventEmitter();
  @Output() update: EventEmitter<{ address_id: string; new_address: string }> =
    new EventEmitter();

  constructor() {}

  ngOnInit(): void {}

  onDelete(address_is: string) {
    this.delete.emit(address_is);
  }

  onUpdate(a: Address, new_address: string) {
    if (a.address == new_address) {
      return;
    }
    this.update.emit({ address_id: a.address_id, new_address });
  }
}
