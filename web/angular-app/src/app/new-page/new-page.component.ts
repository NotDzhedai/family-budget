import { Component, OnInit } from "@angular/core";
import { NewPayment } from "../shared/data/payment/interface";
import { AddressService } from "../shared/services/address.service";
import { PaymentService } from "../shared/services/payment.service";

@Component({
  selector: "app-new-page",
  templateUrl: "./new-page.component.html",
  styleUrls: ["./new-page.component.css"],
})
export class NewPageComponent implements OnInit {
  constructor(
    public addressService: AddressService,
    public paymentService: PaymentService
  ) {}

  ngOnInit(): void {
    this.paymentService.clearErrors();

    this.addressService.getAddresses();
  }

  onDelete(address_id: string) {
    this.addressService.deleteAddress(address_id);
  }

  onUpdate(data: { address_id: string; new_address: string }) {
    this.addressService.updateAddress(data.address_id, data.new_address);
  }

  onCreateAddress(addr: string) {
    this.addressService.createAddress(addr);
  }

  onCreatePayment(np: NewPayment) {
    this.paymentService.createPayment(np);
  }
}
