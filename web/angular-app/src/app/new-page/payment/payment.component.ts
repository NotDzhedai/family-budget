import {
  AfterContentInit,
  AfterViewInit,
  Component,
  EventEmitter,
  Input,
  OnInit,
  Output,
} from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { Subject, Subscription, takeUntil } from "rxjs";
import { Address } from "src/app/shared/data/address/interface";
import { Err } from "src/app/shared/data/error/Err";
import { NewPayment } from "src/app/shared/data/payment/interface";
import { AddressService } from "src/app/shared/services/address.service";
import { PaymentService } from "src/app/shared/services/payment.service";

@Component({
  selector: "app-payment",
  templateUrl: "./payment.component.html",
  styleUrls: ["./payment.component.css"],
})
export class PaymentComponent implements OnInit {
  @Output() create: EventEmitter<NewPayment> = new EventEmitter();

  @Input() addresses: Address[];
  @Input() loading: boolean;
  @Input() err: Err;

  notifier = new Subject<void>();
  defaultAddrSelect: Address;

  paymentForm: FormGroup;

  constructor(private fb: FormBuilder) {}

  ngOnInit(): void {
    this.paymentForm = this.fb.group({
      address: this.fb.control(null, [Validators.required]),
      gas: this.fb.control(null, [Validators.required, Validators.min(0)]),
      gas_transit: this.fb.control(null, [
        Validators.required,
        Validators.min(0),
      ]),
      light: this.fb.control(null, [Validators.required, Validators.min(0)]),
      water: this.fb.control(null, [Validators.required, Validators.min(0)]),
      waste: this.fb.control(null, [Validators.required, Validators.min(0)]),
      apartment_fee: this.fb.control(null, [
        Validators.required,
        Validators.min(0),
      ]),
      elevator: this.fb.control(null, [Validators.required, Validators.min(0)]),
      intercom: this.fb.control(null, [Validators.required, Validators.min(0)]),
      heating_fee: this.fb.control(null, [
        Validators.required,
        Validators.min(0),
      ]),
      heating_subscription_fee: this.fb.control(null, [
        Validators.required,
        Validators.min(0),
      ]),
    });

    this.defaultAddrSelect = this.addresses[0];
    this.paymentForm.controls["address"].setValue(this.defaultAddrSelect);
  }

  onSubmit() {
    this.paymentForm.disable();

    var address: Address = this.paymentForm.controls["address"].value;
    var np: NewPayment = {
      address_id: address.address_id,
      gas: this.paymentForm.controls["gas"].value,
      gas_transit: this.paymentForm.controls["gas_transit"].value,
      light: this.paymentForm.controls["light"].value,
      water: this.paymentForm.controls["water"].value,
      waste: this.paymentForm.controls["waste"].value,
      apartment_fee: this.paymentForm.controls["apartment_fee"].value,
      elevator: this.paymentForm.controls["elevator"].value,
      intercom: this.paymentForm.controls["intercom"].value,
      heating_fee: this.paymentForm.controls["heating_fee"].value,
      heating_subscription_fee:
        this.paymentForm.controls["heating_subscription_fee"].value,
    };

    this.create.emit(np);

    this.paymentForm.reset();
    this.paymentForm.enable();

    this.paymentForm.controls["address"].setValue(this.defaultAddrSelect);
  }

  ngOnDestroy(): void {
    this.notifier.next();
    this.notifier.complete();
  }

  get address() {
    return this.paymentForm.get("address");
  }

  get gas() {
    return this.paymentForm.get("gas");
  }

  get gas_transit() {
    return this.paymentForm.get("gas_transit");
  }

  get light() {
    return this.paymentForm.get("light");
  }

  get water() {
    return this.paymentForm.get("water");
  }

  get waste() {
    return this.paymentForm.get("waste");
  }

  get apartment_fee() {
    return this.paymentForm.get("apartment_fee");
  }

  get elevator() {
    return this.paymentForm.get("elevator");
  }

  get intercom() {
    return this.paymentForm.get("intercom");
  }

  get heating_fee() {
    return this.paymentForm.get("heating_fee");
  }

  get heating_subscription_fee() {
    return this.paymentForm.get("heating_subscription_fee");
  }
}
