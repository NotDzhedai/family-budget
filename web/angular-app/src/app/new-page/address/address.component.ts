import {
  Component,
  ElementRef,
  EventEmitter,
  Input,
  OnDestroy,
  OnInit,
  Output,
  ViewChild,
} from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { Err } from "src/app/shared/data/error/Err";
import { AddressService } from "src/app/shared/services/address.service";

@Component({
  selector: "app-address",
  templateUrl: "./address.component.html",
  styleUrls: ["./address.component.css"],
})
export class AddressComponent implements OnInit {
  @Output() create: EventEmitter<string> = new EventEmitter();

  @Input() loading: boolean;
  @Input() err: Err;

  addressForm: FormGroup;

  constructor(private fb: FormBuilder) {}

  onSubmit() {
    if (this.addressForm.invalid) {
      return;
    }

    this.addressForm.disable();

    this.create.emit(this.addressForm.controls["address"].value);

    this.addressForm.reset();
    this.addressForm.enable();
  }

  ngOnInit(): void {
    this.addressForm = this.fb.group({
      address: this.fb.control(null, [
        Validators.required,
        Validators.minLength(4),
      ]),
    });
  }

  get address() {
    return this.addressForm.get("address");
  }
}
