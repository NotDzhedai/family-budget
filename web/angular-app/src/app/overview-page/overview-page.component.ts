import { AfterViewInit, Component, OnDestroy, OnInit } from "@angular/core";
import { FormGroup, FormBuilder } from "@angular/forms";
import { Subject, Subscription, takeUntil } from "rxjs";
import { Address } from "../shared/data/address/interface";
import { AddressService } from "../shared/services/address.service";
import { PaymentService } from "../shared/services/payment.service";

@Component({
  selector: "app-overview-page",
  templateUrl: "./overview-page.component.html",
  styleUrls: ["./overview-page.component.css"],
})
export class OverviewPageComponent implements OnInit {
  notifier = new Subject<void>();

  activeAddress: Address;
  form: FormGroup;
  currentPage = 1;
  ROWS_PER_PAGE = 10;

  constructor(
    public payment: PaymentService,
    public address: AddressService,
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.form = this.fb.group({
      address: this.fb.control(null),
    });

    this.address.getAddresses().add(() => {
      this.address.addresses$
        .pipe(takeUntil(this.notifier))
        .subscribe((data) => {
          console.log(data);
          if (data.length != 0) {
            this.activeAddress = data[0];
            this.form.controls["address"].setValue(this.activeAddress);
          }
          this.getPayments();
        });
    });
  }

  onChangeAddress(addr: Address) {
    this.activeAddress = addr;
    this.getPayments();
  }

  getPayments() {
    if (this.activeAddress) {
      this.payment.getPayments(
        this.activeAddress.address_id,
        this.currentPage,
        this.ROWS_PER_PAGE
      );
    } else {
      this.payment.clearPayments();
    }
  }

  nextPage() {
    this.currentPage++;
    this.getPayments();
  }

  prevPage() {
    this.currentPage--;
    this.getPayments();
  }

  ngOnDestroy(): void {
    this.notifier.next();
    this.notifier.complete();
  }
}
