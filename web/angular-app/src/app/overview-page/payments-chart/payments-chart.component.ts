import { Component, Input, OnInit } from "@angular/core";
import { FormBuilder, FormControl, FormGroup } from "@angular/forms";
import { OverviewChartData, ChartItem } from "src/app/shared/data/chart/chart";
import { Payment } from "src/app/shared/data/payment/interface";

@Component({
  selector: "app-payments-chart",
  templateUrl: "./payments-chart.component.html",
  styleUrls: ["./payments-chart.component.css"],
})
export class PaymentsChartComponent implements OnInit {
  @Input() payments: Payment[] = [];
  chartData: OverviewChartData;

  optionForm: FormGroup;
  options: string[];
  currentOption: string;

  data: ChartItem[];

  constructor(private fb: FormBuilder) {}

  ngOnInit(): void {
    this.chartData = new OverviewChartData(this.payments);

    this.options = this.chartData.getSelectOptions();
    this.currentOption = this.options[0];

    this.optionForm = this.fb.group({
      optionCtl: this.fb.control(this.currentOption),
    });

    this.data = this.chartData.getDataForChart(this.currentOption);
  }

  onChange(o: string) {
    this.currentOption = o;
    this.data = this.chartData.getDataForChart(this.currentOption);
  }
}
