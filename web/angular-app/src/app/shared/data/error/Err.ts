export class Err {
  constructor(public error: string, public fields: { [key: string]: string }) {}

  getFieldsLen(): number {
    return Object.keys(this.fields).length;
  }

  genFieldsMessage(): string {
    let res: string = "";

    for (const [key, value] of Object.entries(this.fields)) {
      res += `${key}: ${value}\n`;
    }
    return res;
  }
}
