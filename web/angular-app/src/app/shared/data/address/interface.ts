export interface Address {
  address_id: string;
  user_id: string;
  address: string;
  date_created: Date;
  date_updated: Date;
}
