export interface User {
  id: string;
  name: string;
  email: string;
  password: string;
  password_validation: string;
}

export interface UpdateUser {
  name?: string;
  email?: string;
  password?: string;
  password_validation?: string;
}

export interface NewUser {
  name: string;
  email: string;
  date_created: Date;
  date_updated: Date;
}
