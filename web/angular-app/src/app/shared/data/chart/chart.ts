import { formatDate } from "@angular/common";
import { Payment } from "../payment/interface";

export interface ChartItem {
  name: Date | string;
  value: number;
}

export class OverviewChartData {
  payments: Payment[];
  constructor(p: Payment[]) {
    this.payments = p;
  }

  getSelectOptions(): string[] {
    return Object.keys(this.payments[0]).slice(4);
  }

  getDataForChart(option: any): ChartItem[] {
    return this.payments.map((p, idx) => {
      let v: ChartItem = {
        // add idx to make unique key
        name: `(${idx + 1}) ${formatDate(
          p.date_created,
          "mediumDate",
          "en-US"
        )}`,
        value: Number(p[option as keyof Payment]), // hz...
      };
      return v;
    });
  }
}
export class PaymentChartData {
  payment: Payment;
  constructor(p: Payment) {
    this.payment = p;
  }

  getDataForChart(): ChartItem[] {
    let res: ChartItem[] = [];

    for (const [key, value] of Object.entries(this.payment).slice(4)) {
      let v: ChartItem = {
        name: key,
        value: value,
      };

      res.push(v);
    }

    return res;
  }
}
