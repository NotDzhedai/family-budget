export interface QueryPaymentResponse {
  payments: Payment[];
  pages: number;
}

export interface Payment {
  payment_id: string;
  user_id: string;
  address_id: string;
  date_created: Date;
  gas: number;
  gas_transit: number;
  light: number;
  water: number;
  waste: number;
  apartment_fee: number;
  elevator: number;
  intercom: number;
  heating_fee: number;
  heating_subscription_fee: number;
}

export interface NewPayment {
  address_id: string;
  gas: number;
  gas_transit: number;
  light: number;
  water: number;
  waste: number;
  apartment_fee: number;
  elevator: number;
  intercom: number;
  heating_fee: number;
  heating_subscription_fee: number;
}

export interface UpdatePayment {
  address_id: string;
  gas: number;
  gas_transit: number;
  light: number;
  water: number;
  waste: number;
  apartment_fee: number;
  elevator: number;
  intercom: number;
  heating_fee: number;
  heating_subscription_fee: number;
}
