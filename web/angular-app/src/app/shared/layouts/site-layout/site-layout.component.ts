import { Component, OnInit } from "@angular/core";
import { AuthService } from "../../services/auth.service";

interface Path {
  path: string;
  name: string;
}

@Component({
  selector: "app-site-layout",
  templateUrl: "./site-layout.component.html",
  styleUrls: ["./site-layout.component.css"],
})
export class SiteLayoutComponent implements OnInit {
  links: Path[] = [
    { path: "overview", name: "Overview" },
    { path: "new", name: "New address / payment" },
  ];
  constructor(private auth: AuthService) {}

  ngOnInit(): void {}

  logout(): void {
    this.auth.logOut();
  }
}
