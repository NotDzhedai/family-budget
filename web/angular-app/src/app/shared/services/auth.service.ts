import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Router } from "@angular/router";
import { Observable, tap } from "rxjs";
import { NewUser, User } from "../data/user/interface";

@Injectable({
  providedIn: "root",
})
export class AuthService {
  addr: string = "http://localhost:3000/v1";

  constructor(private http: HttpClient, private router: Router) {}

  private token: string | null = null;

  register(nu: NewUser): Observable<User> {
    return this.http.post<User>(this.addr + "/users", nu);
  }

  login(email: string, password: string): Observable<{ token: string }> {
    return this.http
      .get<{ token: string }>(this.addr + "/users/token", {
        headers: {
          Authorization: "Basic " + btoa(`${email}:${password}`),
          skip: "true", // skip token interceptor
        },
      })
      .pipe(
        tap(({ token }) => {
          localStorage.setItem("auth-token", token);
          this.setToken(token);
        })
      );
  }

  setToken(token: string | null) {
    this.token = token;
  }

  getToken(): string | null {
    return this.token;
  }

  isAuthenticated(): boolean {
    return !!this.token;
  }

  logOut() {
    this.setToken(null);
    localStorage.clear();
    this.router.navigate(["/login"], {
      queryParams: {
        logout: true,
      },
    });
  }
}
