import { HttpClient, HttpErrorResponse } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { BehaviorSubject, Observable, Subscription } from "rxjs";
import { Err } from "../data/error/Err";
import {
  NewPayment,
  Payment,
  QueryPaymentResponse,
} from "../data/payment/interface";

@Injectable({
  providedIn: "root",
})
export class PaymentService {
  addr: string = "http://localhost:3000/v1";

  // payments
  private payments: BehaviorSubject<Payment[]> = new BehaviorSubject<Payment[]>(
    []
  );
  payments$: Observable<Payment[]> = this.payments.asObservable();

  // payment
  private payment: BehaviorSubject<Payment | null> =
    new BehaviorSubject<Payment | null>(null);
  payment$: Observable<Payment | null> = this.payment.asObservable();

  // pages
  private pages: BehaviorSubject<number> = new BehaviorSubject<number>(0);
  pages$: Observable<number> = this.pages.asObservable();

  // loading
  private loading: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(
    false
  );
  loading$: Observable<boolean> = this.loading.asObservable();

  // error
  private error: BehaviorSubject<Err | null> = new BehaviorSubject<Err | null>(
    null
  );
  error$: Observable<Err | null> = this.error.asObservable();

  constructor(private http: HttpClient) {}

  createPayment(np: NewPayment): Subscription {
    this.loading.next(true);
    this.error.next(null);
    return this.http.post<Payment>(this.addr + "/payment", np).subscribe({
      next: () => this.loading.next(false),
      error: (error) => {
        this.loading.next(false);
        this.error.next(handleErr(error));
      },
    });
  }

  getPayments(addrID: string, page: number, rowsPerPage: number): Subscription {
    this.loading.next(true);
    this.error.next(null);
    return this.http
      .get<QueryPaymentResponse>(
        this.addr + `/payments/${addrID}/${page}/${rowsPerPage}`,
        {}
      ) // rows per page = 10!
      .subscribe({
        next: (data) => {
          this.payments.next(data.payments);
          this.pages.next(data.pages);
          this.loading.next(false);
          console.log(data); // ! temp
        },
        error: (error) => {
          this.error.next(handleErr(error));
          this.loading.next(false);
        },
      });
  }

  clearPayments() {
    this.payments.next([]);
    this.pages.next(0);
  }

  getSinglePayment(pmtID: string): Subscription {
    this.error.next(null);
    this.loading.next(true);
    return this.http
      .get<Payment>(this.addr + `/payment/${pmtID}`) // rows per page = 10!
      .subscribe({
        next: (data) => {
          this.payment.next(data);
          this.loading.next(false);
        },
        error: (error) => {
          this.error.next(handleErr(error));
          this.loading.next(false);
        },
      });
  }

  clearErrors() {
    this.error.next(null);
  }
}

function handleErr(error: HttpErrorResponse): Err {
  let err: Err;
  switch (error.status) {
    case 400:
      return new Err(error.error.error, error.error.fields);
    case 404:
      return new Err(
        "[404] Not found such payment for this user",
        error.error.fields
      );
    default:
      return new Err(error.error.error, {});
  }
}
