import { HttpClient, HttpErrorResponse } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { BehaviorSubject, Observable, Subscription } from "rxjs";
import { Address } from "../data/address/interface";
import { Err } from "../data/error/Err";

@Injectable({
  providedIn: "root",
})
export class AddressService {
  addr: string = "http://localhost:3000/v1";

  // addresses
  private addresses: BehaviorSubject<Address[]> = new BehaviorSubject<
    Address[]
  >([]);
  addresses$: Observable<Address[]> = this.addresses.asObservable();

  // loading
  private loading: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(
    false
  );
  loading$: Observable<boolean> = this.loading.asObservable();

  // error
  private error: BehaviorSubject<Err | null> = new BehaviorSubject<Err | null>(
    null
  );
  error$: Observable<Err | null> = this.error.asObservable();

  constructor(private http: HttpClient) {}

  getAddresses(): Subscription {
    this.error.next(null);
    this.loading.next(true);
    return this.http.get<Address[]>(this.addr + "/addresses/1/30").subscribe({
      next: (data) => {
        this.addresses.next(data);
        this.loading.next(false);
      },
      error: (err) => {
        this.error.next(handleErr(err));
        this.loading.next(false);
      },
    });
  }

  createAddress(address: string): Subscription {
    this.error.next(null);
    this.loading.next(true);

    return this.http
      .post<void>(this.addr + "/address", {
        address: address,
      })
      .subscribe({
        next: () => {
          this.loading.next(false);
          this.getAddresses();
        },
        error: (err) => {
          this.error.next(handleErr(err));
          this.loading.next(false);
        },
      });
  }

  deleteAddress(address_id: string): Subscription {
    this.error.next(null);
    this.loading.next(true);
    return this.http
      .delete<void>(this.addr + `/address/${address_id}`)
      .subscribe({
        next: () => {
          this.loading.next(false);
          this.getAddresses();
        },
        error: (err) => {
          this.error.next(handleErr(err));
          this.loading.next(false);
        },
      });
  }

  updateAddress(address_id: string, newAddr: string): Subscription {
    let body = { address: newAddr };

    this.error.next(null);
    this.loading.next(true);
    return this.http
      .put<void>(this.addr + `/address/${address_id}`, body)
      .subscribe({
        next: () => {
          this.loading.next(false);
          this.getAddresses();
        },
        error: (err) => {
          this.error.next(handleErr(err));
          this.loading.next(false);
        },
      });
  }
}

function handleErr(error: HttpErrorResponse): Err {
  let err: Err;
  switch (error.status) {
    case 400:
      return new Err(error.error.error, error.error.fields);
    case 404:
      return new Err(
        "[404] Not found such address for this user",
        error.error.fields
      );
    default:
      return new Err(error.error.error, {});
  }
}
