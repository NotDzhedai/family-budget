import {
  HttpInterceptor,
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpErrorResponse,
} from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Router } from "@angular/router";
import { Observable, catchError, throwError } from "rxjs";
import { AuthService } from "../services/auth.service";

@Injectable()
export class TokenInterceptor implements HttpInterceptor {
  constructor(private auth: AuthService, private router: Router) {}

  intercept(
    req: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    if (req.headers.get("skip")) {
      return next.handle(req);
    }
    if (this.auth.isAuthenticated()) {
      req = req.clone({
        setHeaders: {
          Authorization: "Bearer " + this.auth.getToken()!,
        },
      });
    }
    return next
      .handle(req)
      .pipe(
        catchError((error: HttpErrorResponse) => this.handleAuthError(error))
      );
  }

  private handleAuthError(
    error: HttpErrorResponse
  ): Observable<HttpEvent<any>> {
    if (error.status === 401) {
      this.router.navigate(["/login"], {
        queryParams: {
          tokenExpired: true,
        },
      });
    }
    return throwError(error);
  }
}
