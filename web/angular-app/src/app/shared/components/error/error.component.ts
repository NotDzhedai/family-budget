import { Component, Input, OnInit } from "@angular/core";
import { Err } from "../../data/error/Err";

@Component({
  selector: "app-error",
  templateUrl: "./error.component.html",
  styleUrls: ["./error.component.css"],
})
export class ErrorComponent implements OnInit {
  @Input() err: Err;

  constructor() {}

  ngOnInit(): void {}
}
