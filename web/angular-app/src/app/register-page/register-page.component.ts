import { HttpErrorResponse } from "@angular/common/http";
import { Component, OnDestroy, OnInit } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { Router } from "@angular/router";
import { Subject, takeUntil } from "rxjs";
import { Err } from "../shared/data/error/Err";
import { AuthService } from "../shared/services/auth.service";

@Component({
  selector: "app-register-page",
  templateUrl: "./register-page.component.html",
  styleUrls: ["./register-page.component.css"],
})
export class RegisterPageComponent implements OnInit, OnDestroy {
  notifier = new Subject<void>();

  registerForm: FormGroup;
  err: Err;

  constructor(
    private fb: FormBuilder,
    private auth: AuthService,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.registerForm = this.fb.group({
      name: this.fb.control(null, [
        Validators.required,
        Validators.minLength(4),
      ]),
      email: this.fb.control(null, [Validators.required, Validators.email]),
      password: this.fb.control(null, [
        Validators.required,
        Validators.minLength(5),
      ]),
      password_validation: this.fb.control(null, [
        Validators.required,
        Validators.min(5),
      ]),
    });
  }

  ngOnDestroy(): void {
    this.notifier.next();
    this.notifier.complete();
  }

  onSubmit() {
    this.registerForm.disable();
    this.auth
      .register(this.registerForm.value)
      .pipe(takeUntil(this.notifier))
      .subscribe({
        next: () => {
          this.router.navigate(["/login"], {
            queryParams: {
              registered: true,
            },
          });
        },
        error: (error: HttpErrorResponse) => {
          switch (error.status) {
            case 400:
              this.err = new Err(error.error.error, error.error.fields);
              break;
            default:
              this.err = new Err(error.error.error, {});
              break;
          }
          this.registerForm.reset();
          this.registerForm.enable();
        },
      });
  }

  get name() {
    return this.registerForm.get("name");
  }

  get email() {
    return this.registerForm.get("email");
  }

  get password() {
    return this.registerForm.get("password");
  }

  get password_validation() {
    return this.registerForm.get("password_validation");
  }
}
